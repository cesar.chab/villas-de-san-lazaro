CREATE TABLE api_token (
	id INT auto_increment NOT NULL,
	username varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	token LONGTEXT NULL,
	expira_en DATETIME NULL,
	otorgado_a varchar(100) NULL,
	created_at DATETIME DEFAULT now() NOT NULL,
	updated_at DATETIME DEFAULT now() NOT NULL,
	CONSTRAINT api_token_pk PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;
