CREATE TABLE factura_certificada (
    id INT auto_increment NOT NULL,
    correlativo_recibo varchar(100),
	codigo varchar(100)  NULL,
	mensaje varchar(255) NULL,
	acuseReciboSAT varchar(255) NULL,
	codigosSAT varchar(255) NULL,
	responseDATA1 TEXT NULL,
	responseDATA2 TEXT NULL,
	responseDATA3 TEXT NULL,
	autorizacion varchar(255) NULL,
	serie varchar(100) NULL,
	numero varchar(100) NULL,
	fecha_DTE DATETIME NULL,
	nit_eface varchar(100) NULL,
	nombre_eface varchar(255) NULL,
	nit_comprador varchar(100) NULL,
	nombre_comprador varchar(255) NULL,
	backprocesor varchar(100) NULL,
	fecha_de_certificacion DATETIME NULL,	
	CONSTRAINT factura_certificada_pk PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;