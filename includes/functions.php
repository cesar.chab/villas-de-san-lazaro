<?php
require_once "vendor/autoload.php";
// include "./conexion.php";
date_default_timezone_set('America/Guatemala');

function fechaGuatemala()
{
	$mes = array(
		"", "Enero",
		"Febrero",
		"Marzo",
		"Abril",
		"Mayo",
		"Junio",
		"Julio",
		"Agosto",
		"Septiembre",
		"Octubre",
		"Noviembre",
		"Diciembre"
	);
	return date('d') . " de " . $mes[date('n')] . " de " . date('Y');
}

function generarToken($conexion)
{
	$client = new  \GuzzleHttp\Client(['headers' => ['Content-Type' => 'application/json']]);

	//If this is the case, try to disable the SSL certification:
	$client->setDefaultOption('verify', false);
	// $client->setDefaultOption('debug', true);

	//Url de la api
	$urlApiDigiFact = "https://felgttestaws.digifact.com.gt/gt.com.fel.api.v3/api/login/get_token";

	//Credenciales de la api
	$username = 'GT.000026532204.TESTUSER';
	$password = '$cFw8fq+2';

	//Validamos si existe el token en mi base local
	$query = mysqli_query($conexion, "SELECT * FROM api_token LIMIT 1");
	$data = mysqli_fetch_assoc($query);

	//Si existe el token entonces sesionarlo para uso futuro
	if (!empty($data)) {

		//Validamos si el token ya expiro
		$expiraEn = date('Y-m-d H:i:s', strtotime($data['expira_en']));
		$fechaActual = date('Y-m-d H:i:s');

		if ($expiraEn < $fechaActual) {

			//Token expirado, volver a hacer la peticion get_token
			$response = $client->post($urlApiDigiFact, array(
				'headers' => array('Content-Type' => 'application/json'),
				'json' => array('Username' => $username, 'Password' => $password)
			));

			$response = json_decode($response->getBody(), true);
			$data = $response;

			//Actualizamos el api_token 
			$insertApiToken = mysqli_query($conexion, "UPDATE api_token SET token='{$response['Token']}', expira_en='{$response['expira_en']}', otorgado_a='{$response['otorgado_a']}', updated_at='{$fechaActual}' WHERE id=1");

			//Sesionamos el token
			$_SESSION['token'] = $response['Token'];
		} else {

			//Validamos si el token ya esta sesionado
			if (!isset($_SESSION['token'])) {
				$_SESSION['token'] = $data['token'];
			}
		}
	} else { //No existe data entonces consumir el servicio

		$response = $client->post($urlApiDigiFact, array(
			'headers' => array('Content-Type' => 'application/json'),
			'json' => array('Username' => $username, 'Password' => $password)
		));

		$response = json_decode($response->getBody(), true);
		$insertApiToken = mysqli_query($conexion, "INSERT INTO api_token (username, `password`, token, expira_en, otorgado_a) VALUES('{$username}', '{$password}', '{$response["Token"]}', '{$response["expira_en"]}', '{$response["otorgado_a"]}')");

		//Sesionamos el token
		$_SESSION['token'] = $response['Token'];
		$data = $response;
	}


	//Get Token session
	return $_SESSION['token'];
}

function generateRandomString($strength = 12)
{
	$random_string = '';
	$permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$input_length = strlen($permitted_chars);
	for ($i = 0; $i < $strength; $i++) {
		$random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
		$random_string .= $random_character;
	}
	return $random_string;
}

function timbrarFactura($data = null)
{

	$client = new  \GuzzleHttp\Client(['headers' => ['Content-Type' => 'application/json']]);

	//If this is the case, try to disable the SSL certification:
	// $client->setDefaultOption('verify', false);
	// $client->setDefaultOption('debug', true);

	//Url de la api
	$urlApiDigiFact = "https://felgttestaws.digifact.com.gt/gt.com.fel.api.v3/api/FELRequestV2";

	//Credenciales de la api
	$username = 'GT.000026532204.TESTUSER';
	$password = '$cFw8fq+2';


	$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<dte:GTDocumento xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:dte="http://www.sat.gob.gt/dte/fel/0.2.0" Version="0.1">
		<dte:SAT ClaseDocumento="dte">
			<dte:DTE ID="DatosCertificados">
				<dte:DatosEmision ID="DatosEmision">
					<dte:DatosGenerales Tipo="FACT" FechaHoraEmision="2021-10-20T18:20:00"
						CodigoMoneda="GTQ" />
					<dte:Emisor NITEmisor="26532204" NombreEmisor="Nombre o Razon Social" CodigoEstablecimiento="1"
						NombreComercial="Nombre del establecimiento comercial" AfiliacionIVA="GEN">
						<dte:DireccionEmisor>
							<dte:Direccion>CA GUATEMALA, GUATEMALA</dte:Direccion>
							<dte:CodigoPostal>0100</dte:CodigoPostal>
							<dte:Municipio>GUATEMALA</dte:Municipio>
							<dte:Departamento>GUATEMALA</dte:Departamento>
							<dte:Pais>GT</dte:Pais>
						</dte:DireccionEmisor>
					</dte:Emisor>
					<dte:Receptor NombreReceptor="DIGIFACT SERVICIOS" IDReceptor="77454820">
						<dte:DireccionReceptor>
							<dte:Direccion>GUATEMALA</dte:Direccion>
							<dte:CodigoPostal>01010</dte:CodigoPostal>
							<dte:Municipio>GUATEMALA</dte:Municipio>
							<dte:Departamento>GUATEMALA</dte:Departamento>
							<dte:Pais>GT</dte:Pais>
						</dte:DireccionReceptor>
					</dte:Receptor>
					<dte:Frases>
						<dte:Frase TipoFrase="1" CodigoEscenario="1"/>
						
					</dte:Frases>
					<dte:Items>
						<dte:Item NumeroLinea="1" BienOServicio="B">
							<dte:Cantidad>1.000</dte:Cantidad>
							<dte:UnidadMedida>CA</dte:UnidadMedida>
							<dte:Descripcion>Valvula</dte:Descripcion>
							<dte:PrecioUnitario>1.00</dte:PrecioUnitario>
							<dte:Precio>1.00</dte:Precio>
							<dte:Descuento>0</dte:Descuento>
							<dte:Impuestos>
								<dte:Impuesto>
									<dte:NombreCorto>IVA</dte:NombreCorto>
									<dte:CodigoUnidadGravable>1</dte:CodigoUnidadGravable>
									<dte:MontoGravable>0.89</dte:MontoGravable>
									<dte:MontoImpuesto>0.1071</dte:MontoImpuesto>
								</dte:Impuesto>
							</dte:Impuestos>
							<dte:Total>1.00</dte:Total>
						</dte:Item>
					</dte:Items>
					<dte:Totales>
						<dte:TotalImpuestos>
							<dte:TotalImpuesto NombreCorto="IVA" TotalMontoImpuesto="0.1071"/>
						</dte:TotalImpuestos>
						<dte:GranTotal>1.00</dte:GranTotal>
					</dte:Totales>
				</dte:DatosEmision>
			</dte:DTE>
			<dte:Adenda>
			 <dtecomm:Informacion_COMERCIAL xmlns:dtecomm="https://www.digifact.com.gt/dtecomm" xsi:schemaLocation="https://www.digifact.com.gt/dtecomm">
			   <dtecomm:InformacionAdicional Version="7.1234654163">
				   <dtecomm:REFERENCIA_INTERNA>3B7IUMWYO3B3</dtecomm:REFERENCIA_INTERNA>
				   <dtecomm:FECHA_REFERENCIA>2021-10-20T18:20:00</dtecomm:FECHA_REFERENCIA>
				   <dtecomm:VALIDAR_REFERENCIA_INTERNA>VALIDAR</dtecomm:VALIDAR_REFERENCIA_INTERNA>
				</dtecomm:InformacionAdicional>
				</dtecomm:Informacion_COMERCIAL>
			 </dte:Adenda>   
		</dte:SAT>
	</dte:GTDocumento>';



	//Token expirado, volver a hacer la peticion get_token
	$response = $client->post($urlApiDigiFact, array(
		'headers' => array(
			'Content-Type' => 'application/json',
			"Accept" => "application/json",
			"Cache-Control" => "no-cache",
			"Pragma" =>  "no-cache",
			'Authorization' => "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkdULjAwMDAyNjUzMjIwNC5URVNUVVNFUiIsIm5iZiI6MTYzNDc2NDg5MywiZXhwIjoxNjY1ODY4ODkzLCJpYXQiOjE2MzQ3NjQ4OTMsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDkyMjAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ5MjIwIn0.gLkINUXfunbhlggkSyi_j4MOHcV0V6geGRobhWb9u4U" //. $_SESSION['token']
		),
		//'json' => array('Username' => $username, 'Password' => $password)
		'query' => array('NIT' => 26532204, 'TIPO' => 'CERTIFICATE_DTE_XML_TOSIGN', 'FORMAT' => 'XML', 'USERNAME' => 'TESTUSER'),
		"body" => $xml,
		'debug' => true,
		'verify' => false,
	));

	// $response =json_decode($response->getBody(), true);
	// echo "<pre>";
	// print_r($response);
	// var_dump($response);
	// echo $response->getStatusCode();
        // $response->getHeaders(),
        // $response->getBody(),
        // $response->getProtocolVersion(),
        // $response->getReasonPhrase()
	exit();
}
