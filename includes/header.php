<?php
session_start();
if (empty($_SESSION['active'])) {
	header('location: ../');
}


include "../conexion.php";

//el usuario puede usarse en todas las páginas, es el id

$usuario_id=$_SESSION['idUser'];

$nombre=$_SESSION['nombre'];


//buscamos el usuario
	$query = mysqli_query($conexion, "SELECT * FROM usuario WHERE idusuario=$usuario_id");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							
							while ($data = mysqli_fetch_assoc($query)) { 
								$usuario=$data['usuario'];

}
}


//contamos la cantidad de usuarios registrados
	$query = mysqli_query($conexion, "SELECT * FROM usuario");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							$x=0;
							while ($data = mysqli_fetch_assoc($query)) { 
								$x=$x+1;

}
}
else
{
	$x=0;
}



//contamos la cantidad de vecinos registrados
	$query = mysqli_query($conexion, "SELECT * FROM vecino_inquilino");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							$x2=0;
							while ($data = mysqli_fetch_assoc($query)) { 
								$x2=$x2+1;

}
}
else
{
	$x2=0;
}


//contamos la cantidad de servicios registrados
	$query = mysqli_query($conexion, "SELECT * FROM servicios");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							$x3=0;
							while ($data = mysqli_fetch_assoc($query)) { 
								$x3=$x3+1;

}
}
else
{
	$x3=0;
}



//FECHA GUATEMALA
date_default_timezone_set("America/Guatemala");
$fecha_hoy=date('Y').date('/m').date('/d');




//contamos la cantidad de lecturas registrados en el mes actual


date_default_timezone_set("America/Guatemala");  //fecha actual
$fecha_hoy=date('Y-m-d');

$fechaMesPasado = strtotime ('-1 month', strtotime($fecha_hoy)); //operación de fecha


$fechaMesPasadoDate = date('Y-m-j', $fechaMesPasado);  //fecha anterior



	$query = mysqli_query($conexion, "SELECT * FROM lectura where fecha_lectura<='$fecha_hoy' and fecha_lectura>='$fechaMesPasadoDate' and estado_contabilidad=1 or estado_contabilidad=0");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							$x5=0;
							while ($data = mysqli_fetch_assoc($query)) { 
								$x5=$x5+1;

}
}
else
{
	$x5=0;
}



//contamos la cantidad de cheques rechazados

	$query = mysqli_query($conexion, "SELECT * FROM pagos_realizados where cheque_rechazado=1");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							$x6=0;
							while ($data = mysqli_fetch_assoc($query)) { 
								$x6=$x6+1;

}
}
else
{
	$x6=0;
}

//contamos la cantidad de pagos registros del mes

	 $query = mysqli_query($conexion, "SELECT * FROM pagos_realizados where estado_contabilidad=1 and cheque_rechazado=0");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							$x7=0;
							while ($data = mysqli_fetch_assoc($query)) { 
								$x7=$x7+1;

}
}
else
{
	$x7=0;
}


//contamos la cantidad de lecturas registrados del mes

date_default_timezone_set("America/Guatemala");
$fecha_toma1=date("Y/m/25");
$fecha_toma2=date("Y/m/28");
$fecha_toma3=date("Y/m/31");


	 $query = mysqli_query($conexion, "SELECT * FROM lectura where fecha_lectura>='$fecha_toma1' and fecha_lectura<='$fecha_toma2' || fecha_lectura<='$fecha_toma3'");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							$x8=0;
							while ($data = mysqli_fetch_assoc($query)) { 
								$x8=$x8+1;

}
}
else
{
	$x8=0;
}

?>
<!DOCTYPE html>
<html lang="es">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Villas de San Lázaro</title>

	<!-- Custom styles for this template-->
	<link href="css/sb-admin-2.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">

</head>

<body id="page-top">






	<!-- Image and text -->
<nav class="navbar navbar-light bg-light">

   <a href="index.php"><button type="button" class="btn btn-success">Menú</button></a>




   






<!-- Topbar Navbar -->
					<ul class="navbar-nav ml-auto">

						<div class="topbar-divider d-none d-sm-block"></div>

						<!-- Nav Item - User Information -->
						<li class="nav-item dropdown no-arrow">
							<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span style="color: black; text-align: center;" ><i class="fa fa-user" aria-hidden="true"></i><?php  echo $usuario?></span>
							</a>
							<!-- Dropdown - User Information -->
							<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
								
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="salir.php">
									<i class="fa fa-window-close" aria-hidden="true"></i>
									Salir
								</a>
							</div>
						</li>

					</ul>
</nav>