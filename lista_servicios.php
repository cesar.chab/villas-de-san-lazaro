<?php include_once "includes/header.php"; ?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Servicios</h1>
		<a href="registro_servicio.php" class="btn btn-primary">Nuevo Servicio</a>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="table">
					<thead class="thead-dark">
						<tr>
							<th>No.</th>
							<th>Código del Servicio</th>
							<th>Servicio</th>
							<th>precio</th>
							<th>Tipo de Recibo</th>
							
							<?php if ($_SESSION['rol'] == 1||$_SESSION['rol'] == 6) { ?>
							<th>ACCIONES</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						include "../conexion.php";

						$query = mysqli_query($conexion, "SELECT * FROM servicios");
						$result = mysqli_num_rows($query);
						$n=0;
						if ($result > 0) {
							while ($data = mysqli_fetch_assoc($query)) { ?>
								<tr>
									<td><?php echo $n=$n+1; ?></td>
									<td><?php echo $data['codigo_servicio']; ?></td>
									
									<td><?php echo $data['servicio']; ?></td>
									<td><?php echo "Q".$data['precio']; ?></td>
										<td><?php echo $data['tipo_recibo']; ?></td>
									
										<?php if ($_SESSION['rol'] == 1||$_SESSION['rol'] == 6) { ?>
									<td>
									

										<a href="editar_servicio.php?codservicio=<?php echo $data['codservicio']; ?>" class="btn btn-success"><i class='fas fa-edit'></i></a>

										<form action="eliminar_servicio.php?codservicio=<?php echo $data['codservicio']; ?>" method="post" class="confirmar d-inline">
											<button class="btn btn-danger" type="submit"><i class='fas fa-trash-alt'></i> </button>
										</form>
									</td>
										<?php } ?>
								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>