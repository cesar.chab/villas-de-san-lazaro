<?php include_once "includes/header.php"; ?>

<?php  //terminar las sesiones de cualquier usuario?>
<?php unset($_SESSION["cod_recibo_a"]);?>
<?php unset($_SESSION["cod_recibo_b"]);?>
<?php unset($_SESSION["correlativo_recibo"]);?>



                                      
 <div class="container-fluid">
    <div class="row">
             

</div>
</div>


            
        <!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
     
      <div class="container-fluid">
      	 <h4 class="text-center">Cierres diarios</h4>
    <div class="row">
      <div class="col-md"><a href="reportes_menu.php" class="btn btn-tipoab">Regresar&nbsp;<i class="fa fa-reply" aria-hidden="true"></i></a> </div>
          </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="table">
                    <thead  style="background:  #04394d; color: white ">
                        <tr>
                            <th>No.</th>
                            <th>Fecha de cierre</th>
                                                     
                            <?php if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2 || $_SESSION['rol'] == 3||$_SESSION['rol'] == 6) { ?>
                            <th>ACCIONES</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include "../conexion.php";


   $query_delete = mysqli_query($conexion, "DELETE FROM pagos_realizados WHERE inquilino =''");
  


                        //comparamos el estado de lectura y si el estado actual ya esta en cola entonces ya no mostrar

                        $query = mysqli_query($conexion, "SELECT * FROM cierre WHERE dia_cierre!='Null' ORDER BY dia_cierre asc");
                        $result = mysqli_num_rows($query);
                        $fila=0;
                        if ($result > 0) {
                            while ($data = mysqli_fetch_assoc($query)) { ?>
                                <tr>
                                    <td><?php echo $fila=$fila+1; ?></td>
                                    <td><?php echo $data['dia_cierre']; ?></td>
                                                                      
                                        
                                                                
                            
                                        <?php if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2 || $_SESSION['rol'] == 3||$_SESSION['rol'] == 6) { //si el usuario es administrador?>
                                    <td>
                                    


                                        <a href="pdf_cierres_dias.php?dia_cierre=<?php echo $data['dia_cierre']; ?>" class="btn btn-visualizar">
                                        	<i class="fa fa-eye" aria-hidden="true"></i>Visualizar</a>

                                       
                                    </td>
                                        <?php } ?>
                                </tr>
                        <?php }
                        } ?>
                    </tbody>

                </table>
            </div>

        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


            <?php include_once "includes/footer.php"; ?>
