<?php include_once "includes/header.php";
include "../conexion.php";
if (!empty($_POST)) {
    $alert = "";
    if (empty($_POST['num_casa'])  || empty($_POST['nombre_pro'])) {
        $alert = '<div class="alert alert-danger" role="alert">
                    Todo los campos son obligatorio
                </div>';
    } else {


            


        //datos de propietarios
        $num_casa=$_POST['num_casa'];
        $direccion_casa=$_POST['direccion_casa'];
        $dpi_pro = $_POST['dpi_pro'];
        $nombre_pro = $_POST['nombre_pro'];
        $telefono_pro = $_POST['telefono_pro'];
        $correo_pro = $_POST['correo_pro'];
        $contador_agua = $_POST['num_contador'];
        //datos de inquilinos
        $dpi_inq = $_POST['dpi_inq'];
        $nombre_inq = $_POST['nombre_inq'];
        $telefono_inq = $_POST['telefono_inq'];
        $correo_inq = $_POST['correo_inq'];
        $cant_inq = $_POST['cant_inq'];
        
         $nit = $_POST['nit'];


        $usuario_id = $_SESSION['idUser'];
        $result = 0;

        if (is_numeric($dpi_pro) and $dpi_pro != 0) {
            $query = mysqli_query($conexion, "SELECT * FROM vecino_inquilino where dpi_pro = '$dpi_pro' || num_casa='$num_casa';");
            $result = mysqli_fetch_array($query);
        }
        if ($result > 0) {
            $alert = '<div class="alert alert-danger" role="alert">
                                    El vecino y/o casa ya están registrado.
                                </div>';
        } else {
            $query_insert = mysqli_query($conexion, 
            "INSERT INTO vecino_inquilino(num_casa,direccion_casa, dpi_pro, nombre_pro, telefono_pro,correo_pro,num_contador,dpi_inq, nombre_inq, telefono_inq, correo_inq, cant_inq,nit) 
            values ('$num_casa','$direccion_casa', '$dpi_pro', '$nombre_pro', '$telefono_pro', '$correo_pro', '$contador_agua', '$dpi_inq', '$nombre_inq', '$telefono_inq', '$correo_inq', '$cant_inq','$nit')");
            if ($query_insert) {
                $alert = '<div class="alert alert-success" role="alert">
                                    Vecino Registrado Exitosamente.
                                </div>';
            } else {
                $alert = '<div class="alert alert-danger" role="alert">
                                    Error al intertar registrar el vecino !Intente de Nuevo¡
                            </div>';
            }
        }
    
}
    mysqli_close($conexion);
}
?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Registrar Vecino</h1>
        <a href="lista_vecino.php" class="btn btn-primary">Regresar</a>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-10 m-auto">
            <form action="" method="post" autocomplete="off">
            <div class="row">
            <div class="col-md-11">
                    <?php echo isset($alert) ? $alert : ''; ?>
                 </div>
            </div>
                <div class="row">
                   
                    <div class="col-md-5"> 
                        <fieldset>
                        <legend>Datos de Propietario</legend>
                        <div class="form-group">
                            <label for="nocasa">Número de casa</label>
                            <input type="number" placeholder="Ingrese número de casa" name="num_casa" id="num_casa" class="form-control" required value="<?php echo (!empty($num_casa)) ? $num_casa : ""; ?>">

                        </div>
                         <div class="form-group">
                            <label for="nocasa">Dirección</label>
                            <input type="text" placeholder="Ingrese dirección de casa" name="direccion_casa" id="direccion_casa" class="form-control" value="<?php echo (!empty($direccion_casa)) ? $direccion_casa : ""; ?>">
                            
                        </div>




                        <div class="form-group">
                            <label for="dpi">DPI</label>
                            <input type="number" placeholder="Ingrese DPI" name="dpi_pro" id="dpi_pro" class="form-control"  required value="<?php echo (!empty($dpi_pro)) ? $dpi_pro : ""; ?>"     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="13">
                        </div>
                        <div class="form-group">
                            <label for="nombre_propietario">Nombre Completo</label>
                            <input type="text" placeholder="Ingrese Nombre" name="nombre_pro" id="nombre_pro" class="form-control" required value="<?php echo (!empty($nombre_pro)) ? $nombre_pro: ""; ?>">
                        </div>
                        <div class="form-group">
                            <label for="telefono">Teléfono</label>
                            <input type="text" placeholder="Ingrese Teléfono" name="telefono_pro" id="telefono_pro" class="form-control" value="<?php echo (!empty($telefono_pro)) ? $telefono_pro : ""; ?>">
                        </div>
                        <div class="form-group">
                            <label for="correo_propietario">Correo</label>
                            <input type="email" placeholder="Ingrese Correo" name="correo_pro" id="correo_pro" class="form-control" value="<?php echo (!empty($correo_pro)) ? $correo_pro: ""; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Cantidad_personas">Total de personas en la vivienda</label>
                            <input type="number" placeholder="Total de personas en la vivienda" name="cant_inq" id="cant_inq" class="form-control" value="<?php echo (!empty($cant_inq)) ? $cant_inq : ""; ?>">
                        </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                    <fieldset>
                        <legend>Datos de Inquilino</legend>
                        <div class="form-group">
                            <label for="dpi_inquilino">DPI</label>
                            <input type="number" placeholder="Ingrese DPI" name="dpi_inq" id="dpi_inq" class="form-control" required="" value="<?php echo (!empty($dpi_inq)) ? $dpi_inq : ""; ?>" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="13">
                        </div>
                        <div class="form-group">
                            <label for="nombre_inquilino">Nombre Completo</label>
                            <input type="text" placeholder="Ingrese Nombre" name="nombre_inq" id="nombre_inq" class="form-control" required="" value="<?php echo (!empty($nombre_inq)) ? $nombre_inq : ""; ?>">
                        </div>
                        <div class="form-group">
                            <label for="telefono">Teléfono</label>
                            <input type="text" placeholder="Ingrese Teléfono" name="telefono_inq" id="telefono_inq" class="form-control" value="<?php echo (!empty($telefono_inq)) ? $telefono_inq: ""; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Correo">Correo</label>
                            <input type="email" placeholder="Ingrese Correo" name="correo_inq" id="correo_inq" class="form-control" value="<?php echo (!empty($correo_inq)) ? $correo_inq : ""; ?>">
                        </div>
                        
                        <div class="form-group">
                            <label for="numero_contador">Numeró de contador Agua</label>
                            <input type="number" placeholder="Ingrese Numero de contador" name="num_contador" id="num_contador" class="form-control"value="<?php echo (!empty($num_contador)) ? $num_contador : ""; ?>">
                        </div>
                        
                        
                        
                           
                        <div class="form-group">
                            <label for="numero_contador">NIT para el recibo</label>
                            <input type="text" placeholder="Ingrese Número de NIT" name="nit" id="num_nit" class="form-control"value="<?php echo (!empty($nit)) ? $nit : ""; ?>" style="background: #c3ead6 ">
                        </div>
                        
                    </fieldset>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="submit" value="Guardar Datos" class="btn btn-primary" name="guardar_datos">
                        
                    </div>                            
                </div>
            </form>
        </div>
    </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>