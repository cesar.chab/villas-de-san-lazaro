<?php
include_once "includes/header.php";
include "../conexion.php";
if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['servicio']) || empty($_POST['precio'])) {
    $alert = '<div class="alert alert-primary" role="alert">
              Todo los campos son requeridos
            </div>';
  } else {
    $codservicio = $_GET['codservicio'];

    $servicio = $_POST['servicio'];
    $precio = $_POST['precio'];
    $tipo_recibo = $_POST['tipo_recibo'];



    $query_update = mysqli_query($conexion, "UPDATE servicios SET servicio = '$servicio', precio= $precio, tipo_recibo='$tipo_recibo' WHERE codservicio = $codservicio");
    if ($query_update) {
      $alert = '<div class="alert alert-primary" role="alert">
              Servicio modificado exitosamente.
            </div>';
    } else {
      $alert = '<div class="alert alert-primary" role="alert">
                Error al modificar el servicio.
              </div>';
    }
  }
}

// Validar producto

if (empty($_REQUEST['codservicio'])) {
  header("Location: lista_servicios.php");
} else {
  $codservicio = $_REQUEST['codservicio'];
  if (!is_numeric($codservicio)) {
    header("Location: lista_servicios.php");
  }
  $query_servicio = mysqli_query($conexion, "SELECT * FROM servicios where codservicio=$codservicio");
  $result_servicio = mysqli_num_rows($query_servicio);

  if ($result_servicio > 0) {
    $data_servicio = mysqli_fetch_assoc($query_servicio);
  } else {
    header("Location: lista_servicios.php");
  }
}
?>
<!-- Begin Page Content -->
<div class="container-fluid">

   <!-- Page Heading -->
   <div class="d-sm-flex align-items-center justify-content-between mb-4">
     <h1 class="h3 mb-0 text-gray-800">Modificar</h1>
     <a href="lista_servicios.php" class="btn btn-primary">Regresar</a>
   </div>


  <div class="row">
    <div class="col-lg-6 m-auto">

      <div class="card">
        <div class="card-header bg-primary text-white">
          Modificar Servicio
        </div>
        <div class="card-body">
          <form action="" method="post">
            <?php echo isset($alert) ? $alert : ''; ?>
           

         <div class="form-group">
           <label for="servicio">Servicio</label>
           <input type="text" placeholder="Ingrese nombre del servicio" name="servicio" id="servicio" class="form-control" value="<?php echo $data_servicio['servicio']; ?>">
         </div>
         <div class="form-group">
           <label for="precio">Precio</label>
           <input type="text" placeholder="Ingrese precio" class="form-control" name="precio" id="precio" value="<?php echo $data_servicio['precio']; ?>">
         </div>


<div class="form-group">
                    <label>Tipo de Recibo</label>
                    <select name="tipo_recibo" id="tipo_recibo" class="form-control">
                        <?php
                        $query_tipo_recibo = mysqli_query($conexion, " select * from tipo_recibo");
                        mysqli_close($conexion);
                        $resultado_tipo_recibo = mysqli_num_rows($query_tipo_recibo);
                        if ($resultado_tipo_recibo > 0) {
                            while ($tipo_recibo_visual = mysqli_fetch_array($query_tipo_recibo)) {
                        ?>
                                <option value="<?php echo $tipo_recibo_visual["tipo_recibo"]; ?>"><?php echo $tipo_recibo_visual["tipo_recibo"] ?></option>
                        <?php

                            }
                        }

                        ?>
                    </select></div>


  

         <input type="submit" value="Actualizar Servicio" class="btn btn-primary">
       </form>
     </div>
   </div>
    </div>
  </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>