<?php include_once "includes/header.php"; ?>

<?php  //terminar las sesiones de cualquier usuario?>
<?php unset($_SESSION["cod_recibo_a"]);?>
<?php unset($_SESSION["cod_recibo_b"]);?>


                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <h4 class="text-center">Agregar pagos</h4>
                          
                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <form method="post" name="form_new_cliente_venta" id="form_new_cliente_venta">
                                   
                           <a href="seleccionar_vecino.php?cod_recibo_a=<?php echo $form=100?>" class="btn btn-tipoab">Recibo <i class="fa fa-check" aria-hidden="true"></i> &nbsp;"A" </a>

                             <a href="seleccionar_vecino.php?cod_recibo_b=<?php echo $form=200?>" class="btn btn-tipoab">Recibo <i class="fa fa-check" aria-hidden="true"></i> &nbsp;"B" </a>
                           
                        </form>


                                </div>                            
                            </div>
                        
                           






            
        <!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h4 class="text-center">Recibos Realizados</h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>No.</th>
                            <th>Número de Casa</th>
                            <th>Inquilino</th>
                            <th>Correlativo recibo</th>
                            <th>Tipo</th>
                            <th>Fecha Procesado</th>
                           

                            <?php if ($_SESSION['rol'] == 1) { ?>
                            <th>ACCIONES</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include "../conexion.php";


   $query_delete = mysqli_query($conexion, "DELETE FROM pagos_realizados WHERE inquilino =''");
  


                        //comparamos el estado de lectura y si el estado actual ya esta en cola entonces ya no mostrar

                        $query = mysqli_query($conexion, "SELECT * FROM pagos_realizados where codcasa>0 and estado_finalizado=1 and estado_contabilidad=1");
                        $result = mysqli_num_rows($query);
                        $fila=0;
                        if ($result > 0) {
                            while ($data = mysqli_fetch_assoc($query)) { ?>
                                <tr>
                                    <td><?php echo $fila=$fila+1; ?></td>
                                    <td><?php echo $data['codcasa']; ?></td>
                                    <td><?php echo $data['inquilino']; ?></td>
                                    <td><?php echo $data['correlativo_recibo']; ?></td>
                                     <td><?php echo $data['tipo_recibo']; ?></td>
                                    <td><?php echo $data['fecha_procesado']; ?></td>
                                   
                                    
                                        
                                    </td>
                                    
                            
                                        <?php if ($_SESSION['rol'] == 1) { ?>
                                    <td>
                                    


                                        <a href="confirmacion_pago_lectura.php?id=<?php echo $data['idcasa']; ?>" class="btn btn-visualizar">
                                        	<i class="fa fa-eye" aria-hidden="true"></i>Visualizar</a>

                                        <form action="eliminar_anular_recibo.php?idcasa=<?php echo $data['idcasa']; ?>" method="post" class="anular d-inline">
                                            <button class="btn btn-danger" type="submit"><i class="fa fa-times" aria-hidden="true"></i> Anular Recibo</button>
                                        </form>
                                    </td>
                                        <?php } ?>
                                </tr>
                        <?php }
                        } ?>
                    </tbody>

                </table>
            </div>

        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


            <?php include_once "includes/footer.php"; ?>
