<?php
session_start();
require_once "vendor/autoload.php";
include_once('../conexion.php');
include_once('./includes/functions.php');
generarToken($conexion);


$idcasa = $_GET['id'];
$usuario_id = $_SESSION['idUser'];
$nombre = $_SESSION['nombre'];
$usuario = $_SESSION['usuario'];


//BUSCAMOS los datos del usuario código de casa, nombre
$query = mysqli_query($conexion, "SELECT * FROM pagos_realizados where idcasa=$idcasa");
$dataPagos = mysqli_fetch_assoc($query);

// echo "<pre>";
// print_r($dataPagos);
// exit();

$codcasa = null;
$inquilino = null;
$correlativo_recibo = null;
$id_recibo = null;
$cantidad_letras = null;
$fecha_procesado = null;
$valor = null;
$banco = null;
$cheque = null;
$cheque_deposito = null;
$tipo_recibo = null;
$totalPagado = empty($dataPagos) ? 0 : $dataPagos['total_pagado'];

if (!empty($dataPagos)) {
	$codcasa = $dataPagos['codcasa'];
	$inquilino = $dataPagos['inquilino'];
	$correlativo_recibo = $dataPagos['correlativo_recibo']; //correlativo para tipo B
	$id_recibo = $dataPagos['id_recibo']; // correlativo para tipo A
	$cantidad_letras = $dataPagos['total_pagado'];
	$fecha_procesado = $dataPagos['fecha_procesado'];
	$valor = $dataPagos['valor'];
	$banco = $dataPagos['banco'];
	$cheque = $dataPagos['num_che_bol'];
	$cheque_deposito = $dataPagos['cheque_deposito'];
	$tipo_recibo = $dataPagos['tipo_recibo'];
}

//BUSCAMOS los datos del usuario código de casa, nombre  NO TIENE COPIA EN DOS LADOS SOLO AQUÍ PARA LOS DOS
$query_usu = mysqli_query($conexion, "SELECT * FROM vecino_inquilino where num_casa=$codcasa");
$data_usu = mysqli_fetch_assoc($query_usu);
$nombre_pro = null;
$nombre_inq = null;
$direccion_casa = null;
$nit = null;
if (!empty($data_usu)) {
	$nombre_pro = $data_usu['nombre_pro'];
	$nombre_inq = $data_usu['nombre_inq'];
	$nit = $data_usu['nit'];
	$direccion_casa = $data_usu['direccion_casa'];
}

//Obtenemos los parametros
$query = mysqli_query($conexion, "SELECT * FROM parametros where id=1");
$dataParametros = mysqli_fetch_assoc($query);
$precio_basura = null;
$precio_mantenimiento = null;
$precio_seguridad = null;
$tasa_municipal = null;
$precio_agua = null;
$codigo_mantenimiento = null;
if (!empty($dataParametros)) {
	$precio_basura = $dataParametros['precio_basura'];
	$precio_mantenimiento = $dataParametros['precio_mantenimiento'];
	$precio_seguridad = $dataParametros['precio_seguridad'];
	$tasa_municipal = $dataParametros['tasa_municipal'];
	$precio_agua = $dataParametros['codigo_agua'];
	$codigo_mantenimiento = $dataParametros['codigo_mantenimiento']; //es código del servicio
}


//Servicios
$query = mysqli_query($conexion, "SELECT * FROM servicios_tipo_a where codcasa=$codcasa and  correlativo_recibo='$correlativo_recibo'");
$dataServicios = array();
while ($data = mysqli_fetch_assoc($query)) {
	array_push($dataServicios, $data);
}

//

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;

if (isset($_POST) && isset($_POST['es_facturar']) && $_POST['es_facturar'] == 'si') {

	//Validamos si la Recibo ha sido timbrado
	$query = mysqli_query($conexion, "SELECT count(*) as existe FROM factura_certificada where correlativo_recibo='$correlativo_recibo'");
	$dataExiste = mysqli_fetch_assoc($query);

	

	if (!empty($dataExiste) && $dataExiste['existe'] > 0) {		
		?>
		<script>
			alert('No es posible timbrar la factura, porque ha sido certificada anteriormente, favor de validar.');
			window.history.back();
		</script>
		<?php
		return;
	}	

	try {
		$client = new Client(['headers' => ['Content-Type' => 'application/json']]);

		//Url de la api
		$urlApiDigiFact = "https://felgttestaws.digifact.com.gt/gt.com.fel.api.v3/api/FELRequestV2";

		//Credenciales de la api
		$username = 'GT.000026532204.TESTUSER';
		$password = '$cFw8fq+2';

		$fechaEmision = date('c'); //date('Y-m-d H:m:s');

		$referenciaInterna = generateRandomString();

		if (empty($nit)) {
?>
			<script>
				alert('El NIT del receptor no puede ser nulo o vacío');
			</script>
<?php
		}


		$xml = '<?xml version="1.0" encoding="UTF-8"?>
				<dte:GTDocumento xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
					xmlns:dte="http://www.sat.gob.gt/dte/fel/0.2.0" Version="0.1">
					<dte:SAT ClaseDocumento="dte">
						<dte:DTE ID="DatosCertificados">
							<dte:DatosEmision ID="DatosEmision">
								<dte:DatosGenerales Tipo="FACT" FechaHoraEmision="' . $fechaEmision . '"
									CodigoMoneda="GTQ" />
								<dte:Emisor NITEmisor="26532204" NombreEmisor="LA ASOCIACION DE VECINOS DE LA COLONIA VILLAS DE SAN LAZARO" CodigoEstablecimiento="1"
									NombreComercial="LA ASOCIACION DE VECINOS DE LA COLONIA VILLAS DE SAN LAZARO" AfiliacionIVA="GEN">
									<dte:DireccionEmisor>
										<dte:Direccion>1 AVENIDA 2-42 Zona 9, RESIDENCIAL VILLAS SAN LAZARO , San Miguel Petapa</dte:Direccion>
										<dte:CodigoPostal>01066</dte:CodigoPostal>
										<dte:Municipio>SAN MIGUEL PETAPA</dte:Municipio>
										<dte:Departamento>GUATEMALA</dte:Departamento>
										<dte:Pais>GT</dte:Pais>
									</dte:DireccionEmisor>
								</dte:Emisor>
								<dte:Receptor NombreReceptor="' . $nombre_pro . '" IDReceptor="' . $nit . '">
									<dte:DireccionReceptor>
										<dte:Direccion>SAN MIGUEL PETAPA</dte:Direccion>
										<dte:CodigoPostal>01066</dte:CodigoPostal>
										<dte:Municipio>SAN MIGUEL PETAPA</dte:Municipio>
										<dte:Departamento>GUATEMALA</dte:Departamento>
										<dte:Pais>GT</dte:Pais>
									</dte:DireccionReceptor>
								</dte:Receptor>
								<dte:Frases>
									<dte:Frase TipoFrase="1" CodigoEscenario="1"/>									
								</dte:Frases><dte:Items>';

		if (!empty($dataServicios)) {
			$servicio = null;
			$totalImpuestos = 0;
			$granTotal = 0;
			$linea = 1;
			foreach ($dataServicios as $key => $item) {
				$servicio = $item['servicio'];
				$precio = $item['precio'];
				$montoGravable = $precio / 1.12;
				$montoImpuesto = $montoGravable * .12;
				$totalImpuestos += $montoImpuesto;
				$granTotal += $precio;
				$xml .= '<dte:Item NumeroLinea="' . $linea . '" BienOServicio="B">
												<dte:Cantidad>1</dte:Cantidad>
												<dte:UnidadMedida>1</dte:UnidadMedida>
												<dte:Descripcion>' . $servicio . '</dte:Descripcion>
												<dte:PrecioUnitario>' . number_format($precio, 4) . '</dte:PrecioUnitario>
												<dte:Precio>' . number_format($precio, 4) . '</dte:Precio>
												<dte:Descuento>0</dte:Descuento>
												<dte:Impuestos>
													<dte:Impuesto>
														<dte:NombreCorto>IVA</dte:NombreCorto>
														<dte:CodigoUnidadGravable>1</dte:CodigoUnidadGravable>
														<dte:MontoGravable>' . number_format($montoGravable, 4) . '</dte:MontoGravable>
														<dte:MontoImpuesto>' . number_format($montoImpuesto, 4) . '</dte:MontoImpuesto>
													</dte:Impuesto>
												</dte:Impuestos>
												<dte:Total>' . number_format($precio, 4) . '</dte:Total>
											</dte:Item>';
				$linea++;
			}
		}


		$xml .= '</dte:Items><dte:Totales>
									<dte:TotalImpuestos>
										<dte:TotalImpuesto NombreCorto="IVA" TotalMontoImpuesto="' . number_format($totalImpuestos, 4) . '"/>
									</dte:TotalImpuestos>
									<dte:GranTotal>' . number_format($granTotal, 4) . '</dte:GranTotal>
								</dte:Totales>
							</dte:DatosEmision>
						</dte:DTE>
						<dte:Adenda>
						<dtecomm:Informacion_COMERCIAL xmlns:dtecomm="https://www.digifact.com.gt/dtecomm" xsi:schemaLocation="https://www.digifact.com.gt/dtecomm">
						<dtecomm:InformacionAdicional Version="7.1234654163">
							<dtecomm:REFERENCIA_INTERNA>' . $referenciaInterna . '</dtecomm:REFERENCIA_INTERNA>
							<dtecomm:FECHA_REFERENCIA>' . $fechaEmision . '</dtecomm:FECHA_REFERENCIA>
							<dtecomm:VALIDAR_REFERENCIA_INTERNA>VALIDAR</dtecomm:VALIDAR_REFERENCIA_INTERNA>
						</dtecomm:InformacionAdicional>
						</dtecomm:Informacion_COMERCIAL>
						</dte:Adenda>   
					</dte:SAT>
				</dte:GTDocumento>';



		//Consumir Api para hacer el timbrado
		$response = $client->post($urlApiDigiFact, array(
			'headers' => array(
				'Content-Type' => 'application/json',
				"Accept" => "application/json",
				"Cache-Control" => "no-cache",
				"Pragma" =>  "no-cache",
				'Authorization' => "Bearer " . $_SESSION['token']
			),
			//'json' => array('Username' => $username, 'Password' => $password)
			'query' => array('NIT' => 26532204, 'TIPO' => 'CERTIFICATE_DTE_XML_TOSIGN', 'FORMAT' => 'XML,PDF', 'USERNAME' => 'TESTUSER'),
			"body" => $xml,
			'debug' => false,
			'verify' => false, //If this is the case, try to disable the SSL certification:
		));

		$response = json_decode($response->getBody(), true);

		//Afectamos la tabla de factura_certificada siempre y cuando el resultado Codigo = 1
		if (isset($response['Codigo']) && $response['Codigo'] == 1) {
			$insertCFDI = mysqli_query($conexion, "INSERT INTO factura_certificada 
			(correlativo_recibo, codigo, mensaje, acuseReciboSAT, codigosSAT, responseDATA1, responseDATA2, responseDATA3, autorizacion, serie, numero, fecha_DTE, nit_eface, nombre_eface, nit_comprador, nombre_comprador, backprocesor, fecha_de_certificacion) 
			VALUES('{$correlativo_recibo}', '{$response["Codigo"]}', '{$response["Mensaje"]}', '{$response["AcuseReciboSAT"]}', 
			'{$response["CodigosSAT"]}', '{$response["ResponseDATA1"]}', '{$response["ResponseDATA2"]}', '{$response["ResponseDATA3"]}', 
			'{$response["Autorizacion"]}', '{$response["Serie"]}', '{$response["NUMERO"]}', '{$response["Fecha_DTE"]}', '{$response["NIT_EFACE"]}', 
			'{$response["NOMBRE_EFACE"]}', '{$response["NIT_COMPRADOR"]}', '{$response["NOMBRE_COMPRADOR"]}', '{$response["BACKPROCESOR"]}', '{$response["Fecha_de_certificacion"]}');
			");
		}

		?>
		<script>
			let message = `<?=$response['Mensaje']?> con el folio de  Autorización: <?=$response['Autorizacion']?>`
			alert(message)
		</script>
		<?php		
	} catch (RequestException $e) {
		$response = $e->getResponse();
		// $response = json_decode($response->getBody(), true);

		// var_dump((int)$response->getStatusCode()); // HTTP status code;
		// var_dump($response->getReasonPhrase()); // Response message;
		var_dump($response->getBody()->getContents()); // Body, normally it is JSON;
		// var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
		// var_dump($response->getHeaders()); // Headers array;
		// var_dump($response->hasHeader('Content-Type')); // Is the header presented?
		// var_dump($response->getHeader('Content-Type')[0]); // Concrete header value;	
	}
}
?>
<!DOCTYPE html>
<html lang="es">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Imprimir</title>

	<!-- Custom styles for this template-->
	<link href="css/sb-admin-2.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
	<style>
		.td {
			border: 1px black solid;
			padding: 1px;
		}

		.rotate {
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			width: 0em;
		}

		.rotate div {
			-moz-transform: rotate(-90.0deg);
			/* FF3.5+ */
			-o-transform: rotate(-90.0deg);
			/* Opera 10.5 */
			-webkit-transform: rotate(-90.0deg);
			/* Saf3.1+, Chrome */
			filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);
			/* IE6,IE7 */
			-ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)";
			/* IE8 */
			margin-left: -10em;
			margin-right: -10em;
			margin-top: 12em;
		}
	</style>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</head>


<body id="page-top">
	<center>
		<form class="form-inline" method="post">
			<?php


			date_default_timezone_set("America/Guatemala");

			$cont = date('m');

			if ($cont == 1) {
				$mes = "Enero";
			}

			if ($cont == 2) {
				$mes = "Febrero";
			}
			if ($cont == 3) {
				$mes = "Marzo";
			}
			if ($cont == 4) {
				$mes = "Abril";
			}
			if ($cont == 5) {
				$mes = "Mayo";
			}
			if ($cont == 6) {
				$mes = "Junio";
			}
			if ($cont == 7) {
				$mes = "Julio";
			}
			if ($cont == 8) {
				$mes = "Agosto";
			}
			if ($cont == 9) {
				$mes = "Septiembre";
			}
			if ($cont == 10) {
				$mes = "Octubre";
			}
			if ($cont == 11) {
				$mes = "Noviembre";
			}
			if ($cont == 12) {
				$mes = "Diciembre";
			}
			$fecha_hoy = date('d') . " de " . $mes . " del " . date('Y');
			?>


			<table class="table table-responsive">

				<?php

				if ($tipo_recibo == "A") {
					$imagen = "villas_a.png";

					$correlativo_final = $id_recibo;
				}
				if ($tipo_recibo == "B") {
					$imagen = "villas_b.png";
					$correlativo_final = $correlativo_recibo;
				}


				?>

				<tbody>
					<tr>
						<td>
							<img src="<?php echo $imagen ?>" width="900" height="100">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo " Usuario:" . $usuario; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							No.<?php echo $correlativo_final ?>
						</td>
						<td>
							<input type="hidden" name="es_facturar" value="si">
							<button type="submit" class="btn btn-invoice btn-block btn-warning">Facturar</button>
						</td>
					</tr>
					<tr>

						<td style="text-align: left;">No. Casa:<?php echo $codcasa ?></td>
						<td>San Miguel Petapa:<?php

												setlocale(LC_TIME, "spanish");  //converti fecha de inglés a español
												$mi_fecha = $fecha_procesado;
												$mi_fecha = str_replace("/", "-", $mi_fecha);
												$Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
												echo $Nueva_Fecha ?>

						</td>

					</tr>
					<tr>
						<td style="text-align: left;">Propietario(a):<?php echo $nombre_pro ?></td>
						<td>Inquilino:<?php echo $nombre_inq ?></td>
					</tr>
					<tr>
						<td style="text-align: left;">Dirección:<?php echo "San Miguel Petapa" ?></td>
						<td><?php echo  "NIT: " . $nit ?></td>
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered">
				<tr>
					<td class='rotate' rowspan="7">
						<div>
							<p style="font-size:11px">EXENTO DE IMPUESTOS SEGÚN RESOLUCIÓN No. IRG-CRC-A-OPG-UA-RP-651-203</p>
						</div>
					</td>
					<td>cantidad</td>
					<td>Código</td>
					<td colspan="2" style="text-align: center;">Descripción</td>
					<td>Subtotal</td>
				</tr>
				<?php  //Agregar descripción de los excesos y lecturas ordinarios del mes				
				$fecha_hoy = date("Y/m/d");
				$query = mysqli_query($conexion, "SELECT * FROM lectura where codcasa=$codcasa and fecha_lectura<='$fecha_hoy'ORDER BY fecha_lectura ASC");
				$result = mysqli_num_rows($query);
				$total_exceso = 0;
				$total_final_exceso = 0;
				if ($result > 0) {
					while ($data = mysqli_fetch_assoc($query)) {
						$lectura = $data['lectura'];
						$exceso = $data['exceso'];
						$fecha_lectura = $data['fecha_lectura'];
						$lectura_anterior = $data['lectura_anterior'];
						$lectura_actual = $data['lectura_actual'];
					}
				}
				?>

				<?php  //Agregar descripción de los excesos
				$query = mysqli_query($conexion, "SELECT * FROM lectura where codcasa=$codcasa and estado=0 and correlativo_recibo='$correlativo_recibo' and estado_agregado_mes=1");
				$result = mysqli_num_rows($query);
				$total_exceso = 0;
				$total_final_exceso = 0;
				if ($result > 0) {
					while ($data = mysqli_fetch_assoc($query)) { ?>
						<tr>
							<td><?php echo $data['exceso'] . " m³"; ?></td>
							<td><?php echo $data['codigo_servicio'] ?></td>
							<td colspan="2" style="text-align: center;"><?php echo "Exceso," . $data['mes'] . " " . $data['periodo'] ?></td>
							<td>
								<?php
								echo "Q" . $data['cargo'];
								$total_exceso = $total_exceso + ($data['cargo']);
								?>
							</td>

					<?php   }

					$total_final_exceso = $total_exceso;
				} ?>

						</tr>
						<?php  //Agregar pagos de serie A
						$query = mysqli_query($conexion, "SELECT * FROM servicios_tipo_a where codcasa=$codcasa and  correlativo_recibo='$correlativo_recibo'");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							while ($data = mysqli_fetch_assoc($query)) { ?>
								<tr>
									<td align="center">
										<?php echo $cant = 1 ?>
									</td>
									<td><?php echo $cod_servicio = $data['codigo_servicio'] ?></td ?>
									<td colspan="2" style="text-align: center;">
										<?php

										if ($cod_servicio == $codigo_mantenimiento) {

											echo "Cuota(B. " . $precio_basura . ",Tasa M. " . $tasa_municipal . ",Mant. " . $precio_mantenimiento . ",Seguridad " . $precio_seguridad . ",agua " . $precio_agua . ".00)" . ", pago correspondiente a: " . $data['mes'] . "/" . $data['periodo'];
										} else {
											echo $data['servicio'] . ", pago correspondiente a: " . $data['mes'] . "/" . $data['periodo'];
										}

										?>
									</td>

									<td><?php echo "QSD" . $data['precio'] ?></td>

							<?php   }
						} ?>

								</tr>
								<tr>
									<td>Contador</td>
									<td>Lectura Actual:<?php echo (!isset($lectura_actual)) ? '' : $lectura_actual; ?></td>
									<td>Lectura Anterior:<?php echo (!isset($lectura_anterior)) ? '' : $lectura_anterior - $lectura; ?> </td>
									<td>Consumo en m³: <?php echo (!isset($lectura)) ? '' : $lectura; ?> </td>


								</tr>


								<?php
								//convertir número a letras



								/**
								 * @author Elvis Vargas  elvisvc85@gmail.com

								 * @package    Convertir numero a Letras

								 */

								require_once 'numeroaletras.php';

								//llamamos a la(s) clases
								$modelonumero = new modelonumero();
								$numeroaletras = new numeroaletras();





								$numero = $cantidad_letras;










								?>





								<tr>
									<td>

										No.</td>
									<td>Efectivo:<?php if ($valor == 0) {
														echo "Si";
													} else {
														echo "No" . " " . " " . " " . " " . " ";
													} ?>


										Depósito:<?php if ($cheque_deposito == 1) {
														echo "Si";
														$titulo_deposito = "Boleta";
													}

													if ($cheque_deposito == 2) {
														echo "No";
														$titulo_deposito = "Cheque";
													}

													if ($cheque_deposito == 0) {
														echo "No";
														$titulo_deposito = "Cheque";
													}



													?>



									</td>
									<td><?php echo $titulo_deposito ?>:<?php if ($cheque > 0) {
																			echo $cheque;
																		} else {
																			echo "--";
																		} ?> </td>
									<td>Banco:<?php
												if ($cheque > 0) {

													echo $banco;
												} else {
													echo "--";
												}




												?>



									</td>
								</tr>
								<tr>
									<td colspan="3">Cantidad en letras:<span style="font-size: 12px"><?php echo $numeroaletras->convertir($numero, 'Quetzales', 'centavos'); ?></span></td>

									<td style="text-align: right;">Total</td>
									<td><?php

										$query = mysqli_query($conexion, "SELECT * FROM servicios_tipo_a where correlativo_recibo='$correlativo_recibo'");

										$total = 0;

										while ($data = mysqli_fetch_assoc($query)) {

											$total = $total + ($data['precio']);
										}

										//variable que se guarda también en la tabla
										$acumulado = $total + $total_final_exceso;
										echo "Q" . $acumulado;

										?>
									</td>

								</tr>
			</table>
			<p>Observaciones: Si su pago es con cheque y este fuera rechazado por cualquier motivo, tendrá un recargo de Q200.00 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recibo Cliente.</p>




			</div>




			<?php exit(); ?>
			<!DOCTYPE html>
			<html lang="es">

			<head>

				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				<meta name="description" content="">
				<meta name="author" content="">

				<title>Imprimir</title>






				<!-- Custom styles for this template-->
				<link href="css/sb-admin-2.min.css" rel="stylesheet">
				<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">

			</head>


			<body id="page-top">


				<?php include_once('../conexion.php'); ?>
				<?php

				$idcasa = $_GET['id'];


				$usuario_id = $_SESSION['idUser'];

				$nombre = $_SESSION['nombre'];


				//buscamos el usuario
				$query = mysqli_query($conexion, "SELECT * FROM usuario WHERE idusuario=$usuario_id");
				$result = mysqli_num_rows($query);
				if ($result > 0) {

					while ($data = mysqli_fetch_assoc($query)) {
						$usuario = $data['usuario'];
					}
				}



				//BUSCAMOS los datos del usuario código de casa, nombre
				$query = mysqli_query($conexion, "SELECT * FROM pagos_realizados where idcasa=$idcasa");
				$result = mysqli_num_rows($query);

				if ($result > 0) {
					while ($data = mysqli_fetch_assoc($query)) {
						$codcasa = $data['codcasa'];
						$inquilino = $data['inquilino'];
						$correlativo_recibo = $data['correlativo_recibo'];
						$cantidad_letras = $data['total_pagado'];
						$fecha_procesado = $data['fecha_procesado'];
						$valor = $data['valor'];
						$banco = $data['banco'];
						$cheque = $data['num_che_bol'];

						$cheque_deposito = $data['cheque_deposito'];
						$tipo_recibo = $data['tipo_recibo'];
					}
				}

				?>


				<?php
				//jalar algunos parámetros
				$query = mysqli_query($conexion, "SELECT * FROM parametros where id=1");
				$result = mysqli_num_rows($query);

				if ($result > 0) {
					while ($data = mysqli_fetch_assoc($query)) {
						$precio_basura = $data['precio_basura'];
						$precio_mantenimiento = $data['precio_mantenimiento'];
						$precio_seguridad = $data['precio_seguridad'];
						$tasa_municipal = $data['tasa_municipal'];
						$precio_agua = $data['codigo_agua'];
						$codigo_mantenimiento = $data['codigo_mantenimiento']; //es código del servicio


					}
				}


				?>


				<center>
					<form class="form-inline" action="" method="post">




						<?php


						date_default_timezone_set("America/Guatemala");

						$cont = date('m');

						if ($cont == 1) {
							$mes = "Enero";
						}

						if ($cont == 2) {
							$mes = "Febrero";
						}
						if ($cont == 3) {
							$mes = "Marzo";
						}
						if ($cont == 4) {
							$mes = "Abril";
						}
						if ($cont == 5) {
							$mes = "Mayo";
						}
						if ($cont == 6) {
							$mes = "Junio";
						}
						if ($cont == 7) {
							$mes = "Julio";
						}
						if ($cont == 8) {
							$mes = "Agosto";
						}
						if ($cont == 9) {
							$mes = "Septiembre";
						}
						if ($cont == 10) {
							$mes = "Octubre";
						}
						if ($cont == 11) {
							$mes = "Noviembre";
						}
						if ($cont == 12) {
							$mes = "Diciembre";
						}


						$fecha_hoy = date('d') . " de " . $mes . " del " . date('Y');

						?>


						<table class="table table-responsive">

							<?php

							if ($tipo_recibo == "A") {
								$imagen = "villas_a.png";

								$correlativo_final = $id_recibo;
							}
							if ($tipo_recibo == "B") {
								$imagen = "villas_b.png";
								$correlativo_final = $correlativo_recibo;
							}


							?>

							<tbody>
								<tr>
									<td colspan="2"><img src="<?php echo $imagen ?>" width="900" height="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo " Usuario:" . $usuario; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No.<?php echo $correlativo_final ?> </td>
								</tr>
								<tr>

									<td style="text-align: left;">No. Casa:<?php echo $codcasa ?></td>
									<td>San Miguel Petapa:<?php

															setlocale(LC_TIME, "spanish");  //converti fecha de inglés a español
															$mi_fecha = $fecha_procesado;
															$mi_fecha = str_replace("/", "-", $mi_fecha);
															$Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
															echo $Nueva_Fecha ?>



									</td>

								</tr>


								<tr>

									<td style="text-align: left;">Propietario(a):<?php echo $nombre_pro ?></td>
									<td>Inquilino:<?php echo $nombre_inq ?></td>
									<!-- <td>NIT:<? php // echo "existe?"
													?></td>-->

								</tr>
								<tr>
									<td style="text-align: left;">Dirección:<?php echo "San Miguel Petapa" ?></td>
									<td><?php echo  "NIT: " . $nit ?></td>

								</tr>

							</tbody>


						</table>



						<table class="table table-bordered">
							<tr>
								<td class='rotate' rowspan="7">
									<div>
										<p style="font-size:11px">EXENTO DE IMPUESTOS SEGÚN RESOLUCIÓN No. IRG-CRC-A-OPG-UA-RP-651-203</p>
									</div>
								</td>




								<td>cantidad</td>
								<td>Código</td>
								<td colspan="2" style="text-align: center;">Descripción</td>
								<td>Subtotal</td>
							</tr>




							<?php  //Agregar descripción de los excesos y lecturas ordinarios del mes


							date_default_timezone_set("America/Guatemala");
							$fecha_hoy = date("Y/m/d");

							$query = mysqli_query($conexion, "SELECT * FROM lectura where codcasa=$codcasa and fecha_lectura<='$fecha_hoy'ORDER BY fecha_lectura ASC");





							$result = mysqli_num_rows($query);
							$total_exceso = 0;
							$total_final_exceso = 0;
							if ($result > 0) {
								while ($data = mysqli_fetch_assoc($query)) {

									$lectura = $data['lectura'];
									$exceso = $data['exceso'];
									$fecha_lectura = $data['fecha_lectura'];
									$lectura_anterior = $data['lectura_anterior'];
									$lectura_actual = $data['lectura_actual'];
								}
							}

							?>





							<?php  //Agregar descripción de los excesos

							$query = mysqli_query($conexion, "SELECT * FROM lectura where codcasa=$codcasa and estado=0 and correlativo_recibo='$correlativo_recibo' and estado_agregado_mes=1");
							$result = mysqli_num_rows($query);
							$total_exceso = 0;
							$total_final_exceso = 0;
							if ($result > 0) {
								while ($data = mysqli_fetch_assoc($query)) { ?>
									<tr>
										<td><?php echo $data['exceso'] . " m³"; ?></td>
										<td><?php echo $data['codigo_servicio'] ?></td>
										<td colspan="2" style="text-align: center;"><?php echo "Exceso," . $data['mes'] . " " . $data['periodo'] ?></td>
										<td><?php echo "Q" . $data['cargo'];
											$total_exceso = $total_exceso + ($data['cargo']);
											?>
										</td>
									</tr>
							<?php   }

								$total_final_exceso = $total_exceso;
							} ?>




							<?php  //Agregar pagos de serie A

							$query = mysqli_query($conexion, "SELECT * FROM servicios_tipo_a where codcasa=$codcasa and  correlativo_recibo='$correlativo_recibo'");
							$result = mysqli_num_rows($query);

							if ($result > 0) {
								while ($data = mysqli_fetch_assoc($query)) { ?>
									<tr>
										<td align="center">
											<?php echo $cant = 1 ?>
										</td>
										<td><?php echo $cod_servicio = $data['codigo_servicio'] ?></td>
										<td colspan="2" style="text-align: center;">
											<?php

											if ($cod_servicio == $codigo_mantenimiento) {

												echo "Cuota(B. " . $precio_basura . ",Tasa M. " . $tasa_municipal . ",Mant. " . $precio_mantenimiento . ",Seguridad " . $precio_seguridad . ",agua " . $precio_agua . ".00)" . ", pago correspondiente a: " . $data['mes'] . "/" . $data['periodo'];
											} else {
												echo $data['servicio'] . ", pago correspondiente a: " . $data['mes'] . "/" . $data['periodo'];
											}

											?>
										</td>

										<td><?php echo "Q" . $data['precio'] ?></td>

								<?php   }
							} ?>

									</tr>
									<tr>
										<td>Contador</td>
										<td>Lectura Actual:<?php echo (!isset($lectura_actual)) ? '' : $lectura_actual; ?></td>
										<td>Lectura Anterior:<?php echo (!isset($lectura_anterior)) ? '' : $lectura_anterior - $lectura; ?> </td>
										<td>Consumo en m³: <?php echo (!isset($lectura)) ? '' : $lectura; ?> </td>


									</tr>


									<?php
									//convertir número a letras



									/**
									 * @author Elvis Vargas  elvisvc85@gmail.com

									 * @package    Convertir numero a Letras

									 */

									require_once 'numeroaletras.php';

									//llamamos a la(s) clases
									$modelonumero = new modelonumero();
									$numeroaletras = new numeroaletras();





									$numero = $cantidad_letras;










									?>





									<tr>
										<td>

											No.</td>
										<td>Efectivo:<?php if ($valor == 0) {
															echo "Si";
														} else {
															echo "No" . " " . " " . " " . " " . " ";
														} ?>


											Depósito:<?php if ($cheque_deposito == 1) {
															echo "Si";
															$titulo_deposito = "Boleta";
														}

														if ($cheque_deposito == 2) {
															echo "No";
															$titulo_deposito = "Cheque";
														}

														if ($cheque_deposito == 0) {
															echo "No";
															$titulo_deposito = "Cheque";
														}



														?>



										</td>
										<td><?php echo $titulo_deposito ?>:<?php if ($cheque > 0) {
																				echo $cheque;
																			} else {
																				echo "--";
																			} ?> </td>
										<td>Banco:<?php
													if ($cheque > 0) {

														echo $banco;
													} else {
														echo "--";
													}




													?>



										</td>
									</tr>
									<tr>
										<td colspan="3">Cantidad en letras:<span style="font-size: 12px"><?php echo $numeroaletras->convertir($numero, 'Quetzales', 'centavos'); ?></span></td>

										<td style="text-align: right;">Total</td>
										<td><?php

											$query = mysqli_query($conexion, "SELECT * FROM servicios_tipo_a where correlativo_recibo='$correlativo_recibo'");

											$total = 0;

											while ($data = mysqli_fetch_assoc($query)) {

												$total = $total + ($data['precio']);
											}

											//variable que se guarda también en la tabla
											$acumulado = $total + $total_final_exceso;
											echo "Q" . $acumulado;

											?>
										</td>

									</tr>
						</table>
						<p>Observaciones: Si su pago es con cheque y este fuera rechazado por cualquier motivo, tendrá un recargo de Q200.00 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recibo Contabilidad.</p>




						</div>


					</form>
				</center>
			</body>
			<script>
				$(document).ready(function() {
					console.log('Loading')
				})
			</script>

			</html>