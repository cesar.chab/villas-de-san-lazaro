<?php
require_once "vendor/autoload.php";
include "../conexion.php";

$client = new  \GuzzleHttp\Client(['headers' => ['Content-Type' => 'application/json']]);

//If this is the case, try to disable the SSL certification:
$client->setDefaultOption('verify', false);
// $client->setDefaultOption('debug', true);

//Url de la api
$urlApiDigiFact = "https://felgttestaws.digifact.com.gt/gt.com.fel.api.v3/api/login/get_token";

//Credenciales de la api
$username = 'GT.000026532204.TESTUSER';
$password = '$cFw8fq+2';

//Validamos si existe el token en mi base local
$query = mysqli_query($conexion, "SELECT * FROM api_token LIMIT 1");
$data = mysqli_fetch_assoc($query);

//Si existe el token entonces sesionarlo para uso futuro
if (!empty($data)) {

    //Validamos si el token ya expiro
    $expiraEn = date('Y-m-d H:i:s', strtotime($data['expira_en']));
    $fechaActual = date('Y-m-d H:i:s');
    
    if ($expiraEn < $fechaActual) {
    
        //Token expirado, volver a hacer la peticion get_token
        $response = $client->post($urlApiDigiFact, array(
            'headers' => array('Content-Type' => 'application/json'),
            'json' => array('Username' => $username, 'Password' => $password)
        ));
        
        $response =json_decode($response->getBody(), true);
        $data = $response;

        //Actualizamos el api_token 
        $insertApiToken = mysqli_query($conexion, "UPDATE api_token SET token='{$response['Token']}', expira_en='{$response['expira_en']}', otorgado_a='{$response['otorgado_a']}', updated_at='{$fechaActual}' WHERE id=1");

        //Sesionamos el token
        $_SESSION['token'] = $response['Token'];
    
    } else {
    
        //Validamos si el token ya esta sesionado
        if (!isset($_SESSION['token'])) {
            $_SESSION['token'] = $data['token'];
        }
    }
} else { //No existe data entonces consumir el servicio
    
    $response = $client->post($urlApiDigiFact, array(
        'headers' => array('Content-Type' => 'application/json'),
        'json' => array('Username' => $username, 'Password' => $password)
    ));
    
    $response =json_decode($response->getBody(), true);
    $insertApiToken = mysqli_query($conexion, "INSERT INTO api_token (username, `password`, token, expira_en, otorgado_a) VALUES('{$username}', '{$password}', '{$response["Token"]}', '{$response["expira_en"]}', '{$response["otorgado_a"]}')");

    //Sesionamos el token
    $_SESSION['token'] = $response['Token'];
    $data = $response;

}


//Get Token session
echo $_SESSION['token'];

// echo "<pre>";
// print_r($data);
exit();