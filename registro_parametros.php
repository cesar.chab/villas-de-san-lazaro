<?php include_once "includes/header.php"; ?>






	<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Parámetros</title>
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>


<?php
//sino está en blanco entonces realizar
if (!empty($_POST)) {

$codigo_mantenimiento=$_POST['codigo_mantenimiento'];
              $codigo_agua=$_POST['codigo_agua']; 
              $precio_basura=$_POST['precio_basura']; 
               $precio_mantenimiento=$_POST['precio_mantenimiento']; 
                $precio_seguridad=$_POST['precio_seguridad']; 


              $mt_agua=$_POST['mt_agua']; 
							$mt_recargo_agua=$_POST['mt_recargo_agua']; 
							$tasa_municipal=$_POST['tasa_municipal']; 
							$mora_fecha=$_POST['mora_fecha']; 
							$monto_mora=$_POST['monto_mora']; 
							$tiempo_corte_agua=$_POST['tiempo_corte_agua']; 
							$reconexion_agua=$_POST['reconexion_agua'];
							$cheque_rechazado=$_POST['cheque_rechazado'];  



$query_update = mysqli_query($conexion, "UPDATE parametros SET codigo_mantenimiento=$codigo_mantenimiento,precio_basura=$precio_basura,precio_mantenimiento=$precio_mantenimiento,precio_seguridad=$precio_seguridad, codigo_agua=$codigo_agua,mt_agua = $mt_agua, mt_recargo_agua= $mt_recargo_agua,tasa_municipal= $tasa_municipal,mora_fecha= $mora_fecha, monto_mora= $monto_mora, tiempo_corte_agua= $tiempo_corte_agua,reconexion_agua= $reconexion_agua, cheque_rechazado= $cheque_rechazado    WHERE id= 1");
    if ($query_update) {
     
$alert='<div class="alert alert-primary" role="alert">
  Datos actualizados correctamente.
</div>'; 

    } else {
      $alert = '<div class="alert alert-primary" role="alert">
                Error al Modificar
              </div>';
    }

}



//Enlistamos parámetros
//Nota solo mostrar y editar. Guardar de forma manual a la BD
	$query = mysqli_query($conexion, "SELECT * FROM parametros where id=1");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							
							while ($parametros = mysqli_fetch_assoc($query)) {
                  $codigo_mantenimiento=$parametros['codigo_mantenimiento'];

                    $precio_basura=$parametros['precio_basura'];
                      $precio_mantenimiento=$parametros['precio_mantenimiento'];
                        $precio_seguridad=$parametros['precio_seguridad'];
  $codigo_agua=$parametros['codigo_agua']; 
							$mt_agua=$parametros['mt_agua']; 
							$mt_recargo_agua=$parametros['mt_recargo_agua']; 
							$tasa_municipal=$parametros['tasa_municipal']; 
							$mora_fecha=$parametros['mora_fecha']; 
							$monto_mora=$parametros['monto_mora']; 
							$tiempo_corte_agua=$parametros['tiempo_corte_agua']; 
							$reconexion_agua=$parametros['reconexion_agua'];
							$cheque_rechazado=$parametros['cheque_rechazado'];  

								
							}
								
}
?>

<!-- Begin Page Content -->
        <div class="container-fluid">


              <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Configurar Parámetros</h1>
       
    </div>

          <div class="row">
            <div class="col-lg-6 m-auto">

              <form class="" action="" method="post">
              	<?php echo isset($alert) ? $alert : ''; ?>

                  <!--  Código es el precio-->


                <label for="codigo_mantenimiento">Código del Mantenimiento
                  <input type="text" placeholder="Ingrese código" name="codigo_mantenimiento" id="codigo_mantenimiento" class="form-control" value="<?php echo $codigo_mantenimiento?>" required autocomplete="off"></label>
                   <div class="form-group">

                     <label for="precio_basura">Precio Basura
                  <input type="text" placeholder="Ingrese precio" name="precio_basura" id="precio_basura" class="form-control" value="<?php echo $precio_basura?>" required autocomplete="off"></label>


                     <label for="precio_mantenimiento">Precio Mantenimiento
                  <input type="text" placeholder="Ingrese precio" name="precio_mantenimiento" id="precio_mantenimiento" class="form-control" value="<?php echo $precio_mantenimiento?>" required autocomplete="off"></label>

                     <label for="precio_seguridad">Precio Seguridad
                  <input type="text" placeholder="Ingrese precio" name="precio_seguridad" id="precio_seguridad" class="form-control" value="<?php echo $precio_seguridad?>" required autocomplete="off"></label>
           
   
              <!--  Código es el precio-->
                   <label for="codigo_agua">Código del agua
                  <input type="text" placeholder="Ingrese metros" name="codigo_agua" id="codigo_agua" class="form-control" value="<?php echo $codigo_agua?>" required autocomplete="off"></label>
                
                  
                  <label for="mt_agua">Consumo máximo en m³
                  <input type="text" placeholder="Ingrese metros" name="mt_agua" id="mt_agua" class="form-control" value="<?php echo $mt_agua?>" required></label>
                

                  <label for="mt_recargo_agua">Recargo por m³
                  <input type="text" placeholder="Ingrese precio de Recargo" name="mt_recargo_agua" id="mt_recargo_agua" class="form-control" value="<?php echo 	$mt_recargo_agua; ?>" required></label>


                <label for="tasa_municipal">Tasa Municipal
                  <input type="text" placeholder="Ingrese Tasa Municipal" name="tasa_municipal" id="tasa_municipal" class="form-control" value="<?php echo $tasa_municipal;?>" required></label>
                

                  <label for="mora_fecha">Mora a partir del día
                  <input type="number" placeholder="Ingrese fecha" name="mora_fecha" id="mora_fecha" class="form-control" value="<?php echo 	$mora_fecha; ?>" required></label>



                <label for="monto_mora">Monto de Mora
                  <input type="text" placeholder="Ingrese monto" name="monto_mora" id="monto_mora" class="form-control" value="<?php echo $monto_mora;?>" required></label>
                

                  <label for="tiempo_corte_agua">Meses de atraso para recorte
                  <input type="number" placeholder="Ingrese la cantidad" name="tiempo_corte_agua" id="tiempo_corte_agua" class="form-control" value="<?php echo 	$tiempo_corte_agua; ?>" required></label>


       <label for="reconexion_agua">Reconexión de Agua
                  <input type="text" placeholder="Ingrese monto" name="reconexion_agua" id="reconexion_agua" class="form-control" value="<?php echo $reconexion_agua;?>" required></label>
                

                  <label for="cheque_rechazado">Mora Por Cheque Rechazado
                  <input type="text" placeholder="Ingrese la cantidad" name="cheque_rechazado" id="cheque_rechazado" class="form-control" value="<?php echo 	$cheque_rechazado; ?>" required></label>




                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-lock" aria-hidden="true"></i>Guardar Cambios</button>
              </form>
            </div>
          </div>


        </div>
        <!-- /.container-fluid -->

      </div>


</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>



<?php include_once "includes/footer.php"; ?>
</html>