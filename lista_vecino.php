<?php include_once "includes/header.php"; ?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Vecinos</h1>
		<a href="registro_vecino.php" class="btn btn-primary">Nuevo Vecino</a>
	</div>

	<div class="row">
		<div class="col-lg-12">

			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="table">
					<thead class="thead-dark">
						<tr>
							<th>CASA</th>
							<th>Propietario</th>

							<th>Dirección</th>
							<th>DPI propietario</th>
							<th>Tel</th>
							<th>Inquilino</th>
							<th>DPI Inquilino</th>
							<th>Tel</th>
							<th>No. Personas</th>
							<?php if ($_SESSION['rol'] == 1||$_SESSION['rol'] == 2||$_SESSION['rol'] == 6) { ?>
							<th>ACCIONES</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						include "../conexion.php";

						$query = mysqli_query($conexion, "SELECT * FROM vecino_inquilino");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							while ($data = mysqli_fetch_assoc($query)) { ?>
								<tr>
								    
								    <?php 
									
									
									
							
								
								
								$casa=$data['num_casa'];
								
							
								//colores para los vecinos que deben
								$query_contar=mysqli_query($conexion,"SELECT COUNT(codcasa) AS Total FROM lectura Where codcasa=$casa and estado=1");
								($data_contar = mysqli_fetch_assoc($query_contar));
								
							

								$acumulado=$data_contar['Total'];
									
										if($acumulado==2)
									{
									    $color="#eafa8c"; //amarillo si debe dos meses
									    
									}
									
									
									
									if($acumulado==3)   //rojo si debe 3 meses
									{
									    $color=" #ffb6b0 ";
									    
									}
										if($acumulado==1) //verdww si debe solo el mes
									{
									    $color=" #b0ffd4 ";
									    
									}

										if($acumulado==0) //azul si no debe
									{
									    $color="#5e6bf9";
									    
									}
								
								
								
								?>
									<td bgcolor="<?php echo $color?>" style="color:black;">
									<?php 
									
										echo  $data['num_casa'];
									
									?>
									</td>
									<td><?php echo $data['nombre_pro']; ?></td>
										<td><?php echo $data['direccion_casa']; ?></td>
									<td><?php echo $data['dpi_pro']; ?></td>									
									<td><?php echo $data['telefono_pro']; ?></td>
									<td><?php echo $data['nombre_inq']; ?></td>
									<td><?php echo $data['dpi_inq']; ?></td>									
									<td><?php echo $data['telefono_inq']; ?></td>
									<td><?php echo $data['cant_inq']; ?></td>
									<?php if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2||$_SESSION['rol'] == 6) { ?>
									<td>									
										<a href="editar_vecino.php?id=<?php echo $data['cod_vecino'];?>" class="btn btn-success btn-sm">
											<i class='fas fa-edit'></i>										
										</a>
										<form action="eliminar_vecino.php?id=<?php echo $data['cod_vecino']; ?>" method="post" class="confirmar d-inline">
											<button class="btn btn-danger btn-sm" type="submit"><i class='fas fa-trash-alt'></i> </button>
										</form>
										<a href="registro_vehiculo.php?num_casa=<?php echo $data['num_casa'];?>" class="btn btn-primary btn-sm">
											<i class='fas fa-car'></i>										
										</a>
									</td>
									<?php } ?>
								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>