<?php include_once "includes/header.php"; ?>



<!-- Begin Page Content -->
<div class="container-fluid">



	<div class="row">
		<div class="col-lg-12">
			

<center>
<?php  if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2||$_SESSION['rol'] == 6)
{
?>
	<h3>Realizar Cierre</h3>
      <div class="col-md">
      	<!-- DEL DÍA-->
        <button type="button" class="btn btn-primary" style="background:  #1d8b9a " data-toggle="modal" data-target="#exampleModal1">
<i class="fa fa-th" aria-hidden="true"></i>
del Día
</button>



<!-- DEL MES-->
        <button type="button" class="btn btn-primary" style="background:   #1d9a65  "  data-toggle="modal" data-target="#exampleModal2">
<i class="fa fa-th" aria-hidden="true"></i>
del Mes
</button>



<a href="registrar_pagos.php"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
<i class="fa fa-reply" aria-hidden="true"></i>Realizar Pagos</button></a>
    </div>
	
</center>

<?php  } ?>

<!--  CIERRE DEL DÍA-->


<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cierre del Día</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="form-inline" action="" method="post">
      <div class="alert alert-warning" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></i>Se realizará el cierre del día de forma permanente.
</div>

<div class="container-fluid">
    <div class="row">
      <div class="col-md">  
      	<?php 
           // FECHA GUATEMALA

setlocale(LC_TIME, "spanish");
$fecha1 = date("Y/m/d");
$fecha1 = str_replace("/", "-", $fecha1);			
$newDate1 = date("d-m-Y", strtotime($fecha1));				
$nueva_fecha = strftime("%d de %B de %Y", strtotime($newDate1));
?>
<h2>Hoy <?php  echo $nueva_fecha?></h2>


            </div>
    
  

    </div>  
<br>
<br>
 <div class="row">
<div class="col-md">

<button  type="submit" name="confirmar_dia" value="Confirmar Cierre" style="width: 200px;height: 80px;border-radius:  25px;color:white;font-weight:bold;font-size: 20px;background: #104149  " ><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;Confirmar Cierre</button>

        
  </div>
</div>



      
     

    


        <?php
        $aler="";

        if (isset($_POST['confirmar_dia'])) {
  


$codigo_cierre=$fecha1;
$dia_cierre=$fecha1;
$fecha_reporte=$fecha1;
$inicio=$fecha1;
$cierre=$fecha1;
$semana_cierre=$fecha1;

 $query_update = mysqli_query($conexion, "UPDATE pagos_realizados SET dia_cierre='$fecha1' WHERE
 	fecha_procesado='$fecha1' and estado_contabilidad=1 and cheque_rechazado=0");


$query_insert ="INSERT INTO cierre(codigo_cierre,dia_cierre,fecha_reporte,usuario_id)values('$codigo_cierre','$dia_cierre','$fecha_reporte','$usuario_id')";

if (mysqli_query($conexion, $query_insert,$query_update)) {
    

   $alert = '<div class="alert alert-primary" role="alert">
              Cierre realizado con éxito. <i class="fa fa-lock fa-4x" aria-hidden="true"></i>
              </div>';
}

else
{



        ?>
        <?php
              
      
                        
        $alert = '<div class="alert alert-danger" role="alert">
              <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>No es posible realizar dos veces el cierre del día. Los datos ya fueron enviados anteriomente. <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
              </div>'; 
                                  

}
}
?>  
   
      
   
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
   </div>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<center>
<?php
//mensaje si se intenta cerrar dos veces en el día

 echo (isset($alert))? $alert:''  ?>
</center>

<!--  CIERRE DE LA SEMANA -->





<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cierre del Mes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="form-inline" action="cierre_opciones.php" method="post">
      <div class="alert alert-warning" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></i>Se realizará el cierre del mes de forma permanente.
</div>

<div class="container-fluid">
    <div class="row">
      <div class="col-md">  
            Fecha Inicio<input type="date" class="form-control" id="inicio" name="inicio" required=""> </div>
    
          <div class="col-md"> Fecha Final <input type="date" class="form-control" id="cierre" name="cierre" required=""></div>
  

    </div>  
<br>
<br>
 <div class="row">
<div class="col-md">

<button  type="submit" vame="confirmar" value="Confirmar Cierre" style="width: 200px;height: 80px;border-radius:  25px;color:white;font-weight:bold;font-size: 20px;background: #104149  " ><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;Confirmar Cierre</button>

        
  </div>
</div>


<?php
        $aler="";

        if (empty($_POST['inicio']) and empty($_POST['cierre'])) {
   
    } else {

	$inicio=$_POST['inicio'];
         $cierre=$_POST['cierre'];


 $query_update = mysqli_query($conexion, "UPDATE pagos_realizados SET  inicio='$inicio', cierre='$cierre',estado_contabilidad=5 WHERE
 	fecha_procesado>='$inicio' and fecha_procesado<='$cierre' and estado_contabilidad=1 and cheque_rechazado=0");

 $query_lectura = mysqli_query($conexion, "UPDATE lectura SET  estado_contabilidad=5 WHERE fecha_finalizado>='$inicio' and fecha_finalizado<='$cierre'");  //cambiamos de estado a las lecturas una vez realizado el cierre



$query_delete = mysqli_query($conexion, "DELETE FROM lectura_rechazados WHERE estado=1"); //eliminamos las lecturas obtenidas y que no fueron rechazadas durante el mes, para hacer espacio en la base de datos.


date_default_timezone_set("America/Guatemala");

$fecha_reporte=date('Y').date('/m').date('/d');  //fecha de hoy
$codigo_cierre=date('m').date('Y');


$query_insert =("INSERT INTO cierre(codigo_cierre,inicio,cierre,fecha_reporte,usuario_id)values('$codigo_cierre','$inicio','$cierre','$fecha_reporte','$usuario_id')");

if (mysqli_query($conexion, $query_insert,$query_update)) {
    

   $alert = '<div class="alert alert-primary" role="alert">
              Cierre realizado con éxito. <i class="fa fa-lock fa-4x" aria-hidden="true"></i>
              </div>';
}

else
{



        ?>
        <?php
              
      
                        
        $alert = '<div class="alert alert-danger" role="alert">
              <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>No es posible realizar dos veces el cierre del mes. Los datos ya fueron enviados anteriomente. <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
              </div>'; 
                                  

}
}
?>  

     

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
   </div>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>




<center>
<?php
//mensaje si se intenta cerrar dos veces en el día

 echo (isset($alert))? $alert:''  ?>
</center>













</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>