<?php include_once "includes/header.php"; ?>



	


<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">

	</div>

	<!-- Content Row -->
	<div class="row">



<?php  if ($_SESSION['rol'] == 6)
{
?>
		<!-- Earnings (Monthly) Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="registro_parametros.php">
			<div class="card border-left-primary shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Parámetros del Sistema<p></div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
						</div>
						<div class="col-auto">
							<i class="fa fa-cogs fa-2x text-gray-500"></i>
						</div>
					</div>
				</div>
			</div>
		</a>






		<!-- Earnings (Monthly) Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="lista_usuarios.php">
			<div class="card border-left-primary shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Usuarios<p></div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $x ." Registrados"; ?></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-user fa-2x text-gray-500"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<?php   } ?>

		<?php  if ($_SESSION['rol'] == 1 ||$_SESSION['rol'] == 2||$_SESSION['rol'] == 6)
{
?>

		<!-- Earnings (Monthly) Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="lista_vecino.php">
			<div class="card border-left-success shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Vecinos</p></div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $x2 ." Registrados"; ?></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-users fa-2x text-gray-500"></i>
						</div>
					</div>
				</div>
			</div>
		</a>
<?php   } ?>

<?php  if ($_SESSION['rol'] == 1||$_SESSION['rol'] == 6)
{
?>
		<!-- Earnings (Monthly) Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="lista_servicios.php">
			<div class="card border-left-info shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px"> Tipos de Servicios</p></div>
							<div class="row no-gutters align-items-center">
								<div class="col-auto">
									<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $x3 ." Registrados"; ?></div>
								</div>
								<div class="col">
									
								</div>
							</div>
						</div>
						<div class="col-auto">
							<i class="fas fa-clipboard-list fa-2x text-gray-500"></i>
						</div>
					</div>
				</div>
			</div>
		</a>
<?php   } ?>

		<?php  if ($_SESSION['rol'] == 1 ||$_SESSION['rol'] == 4||$_SESSION['rol'] == 6)
{
?>
<!-- Pending Requests Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="lista_lectura.php">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Lectura de Agua</p></div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $x8 ." Registrados"; ?>
							




							</div>
						</div>
						<div class="col-auto">
							<i class="fas fa-faucet fa-2x text-gray-500" aria-hidden="true"></i>

						</div>
					</div>
				</div>
			</div>
		</a>
		
<?php   } ?>


		<?php  if ($_SESSION['rol'] == 1 ||$_SESSION['rol'] == 2||$_SESSION['rol'] == 6)
{
?>
		<!-- Pending Requests Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="registrar_pagos.php">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Realizar Pagos</p></div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $x7 ." Registrados"; ?></div>
						</div>
						<div class="col-auto">
							<i class="fa fa-unlock-alt fa-2x text-gray-500"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

	<?php  } ?>


		<?php  if ($_SESSION['rol'] == 1 ||$_SESSION['rol'] == 2 ||$_SESSION['rol'] == 3||$_SESSION['rol'] == 6)
{
?>
		<!-- Pending Requests Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="reportes_menu.php">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Reportes</p></div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-calendar fa-2x text-gray-500" aria-hidden="true"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<?php   } ?>

		<?php  if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2||$_SESSION['rol'] == 6)
{
?>
		<!-- Pending Requests Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="registros_almacenados.php">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Registros Almacenados</p></div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-database fa-2x text-gray-500"></i>
						</div>
					</div>
				</div>
			</div>
		</a>


<?php  } ?>



		<?php  //Módulo para el usuario

		if ($_SESSION['rol']== 5  )
{
?>
		<!-- Pending Requests Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="usuario_del_vecino.php">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Visualizar tus pagos</p></div> <?php ?>
							<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-user fa-2x text-gray-500"></i>

						</div>
					</div>
				</div>
			</div>
		</a>


<?php  } ?>



		<?php  //Modulo para el administrador y operador

		 if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2||$_SESSION['rol'] == 6)
{
?>
		<!-- Pending Requests Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="usuario_del_vecino_admin.php">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Usuario del Vecino</p></div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-user fa-2x text-gray-500"></i>

						</div>
					</div>
				</div>
			</div>
		</a>


<?php  } ?>

<!-- <a class="col-xl-3 col-md-6 mb-4" href="webservice.php">
	<div class="card border-left-warning shadow h-100 py-2">
		<div class="card-body">
			<div class="row no-gutters align-items-center">
				<div class="col mr-2">
						<div class="text-xs font-weight-bold text-info text-uppercase mb-4"><p style="font-size: 17px">Consumir Api</p></div>
					<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
				</div>
				<div class="col-auto">
					<i class="fas fa-user fa-2x text-gray-500"></i>

				</div>
			</div>
		</div>
	</div>
</a> -->


		
	</div>
	


<?php include_once "includes/footer.php"; ?>