<?php 
    error_reporting(0);
include_once "includes/header.php";
include "../conexion.php";
$num_casa=$_GET['num_casa'];
if (!empty($_POST)) {
    $alert = "";
    if ( empty($_POST['placa']) || empty($_POST['marca']) || empty($_POST['color']) || empty($_POST['modelo'])) {
        $alert = '<div class="alert alert-danger" role="alert">
                    Todo los campos son obligatorio
                  </div>';
    } else {
        $num_casa=$_GET['num_casa'];

        $tipo_vehiculo = strtoupper($_POST['tipovehiculo']);
        $casa = strtoupper($_POST['num_casa']);
        $placa = strtoupper($_POST['placa']);
        $marca = strtoupper($_POST['marca']);
        $color = strtoupper($_POST['color']);
        $modelo = strtoupper($_POST['modelo']);
        $usuario_id = $_SESSION['idUser'];
        $result = 0;
        if ($placa != '') {
            $query = mysqli_query($conexion, "SELECT * FROM vehiculos  where placa = '$placa';");
            $result = mysqli_fetch_array($query);
        }
        if ($result > 0) {
            $alert = '<div class="alert alert-danger" role="alert">
                        Placa ya registrada.
                      </div>';
        } else {
            $query_insert = mysqli_query($conexion, "INSERT INTO vehiculos(num_casa,tipo_vehiculo, placa, marca, modelo, color) 
                                        values ('$casa', '$tipo_vehiculo', '$placa', '$marca', '$modelo','$color')");
            if ($query_insert) {
                $alert = '<div class="alert alert-success" role="alert">
                                Vehículo registrado existosamente
                          </div>';
            } else {
                $alert = '<div class="alert alert-danger" role="alert">
                                    Error al Guardar
                            </div>';
            }
        }
    }
    mysqli_close($conexion);
}
?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Registrar Vehículos</h1>
        <a href="lista_vecino.php" class="btn btn-primary">Regresar</a>
    </div>
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-12 col-md-6">
            <div class="row">
                <div class="col-md-3">
                    <?php echo isset($alert) ? $alert : ''; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-body">
                        <h4 class="text-primary text-center">DATOS VEHÍCULO</h4>
                        <form action="" method="POST" autocomplete="off">
                            <div class="form-group">
                                <label for="num_casa">NÚMERO DE CASA</label>
                                <input type="number" placeholder="Número de casa" name="num_casa" id="num_casa" class="form-control" value="<?php echo $num_casa?>" readonly>
                            </div>
                            <div class="form-group">
                                <label for="vehiculo">TIPO</label>
                                <select class="form-group" aria-label="Default select example" name="tipovehiculo">
                                    <option selected value="automovil">AUTOMÓVIL</option>
                                    <option value="moto">MOTOCICLETA</option>
                                </select>
                            </div>
                            <div class="form-group">
                                
                                <input type="text" placeholder="Número de Placa sin guión" name="placa" id="placa_uno" class="form-control" style="text-transform:uppercase;">
                            </div>
                            <div class="form-group">
                                
                                <input type="text" placeholder="Marca del vehículo  " name="marca" id="marca_uno" class="form-control" style="text-transform:uppercase;">
                            </div>
                            <div class="form-group">                            
                                <input type="text" placeholder="Color del vehículo" name="color" id="color_uno" class="form-control" style="text-transform:uppercase;">
                            </div>
                            <div class="form-group">        
                                <input type="number" placeholder="Modelo del Vehículo" name="modelo" id="modelo" class="form-control" style="text-transform:uppercase;">
                            </div>
                            <div class="form-group">
                                
                                <input type="submit" name="enviar_datos" class="btn btn-primary" value="Registrar Vehículo">
                                <input type="reset" name="Limpiar" class="btn btn-danger" value="Limpiar">          
                            </div>
                        </form>
                    </div>
                                
                </div>
            <div class="col-md-9">
                <div class="table-responsive">
				    <table class="table table-striped table-bordered" id="table">
					<thead class="thead-dark">
						<tr>
                            <th>CÓDIGO</th>
							<th>CASA</th>
							<th>TIPO VEHÍCULO</th>
							<th>PLACA</th>
							<th>MARCA</th>
							<th>MODELO</th>
							<th>COLOR</th>							
							<?php if ($_SESSION['rol'] == 1||$_SESSION['rol'] == 6) { ?>
							<th>ACCIONES</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						include "../conexion.php";

						$query = mysqli_query($conexion, "SELECT * FROM vehiculos WHERE num_casa='$num_casa';");
						$result = mysqli_num_rows($query);
						if ($result > 0) {
							while ($data = mysqli_fetch_assoc($query)) { ?>
								<tr>
                                    <td><?php echo $data['cod_vehiculo']; ?></td>
									<td><?php echo $data['num_casa']; ?></td>
									<td><?php echo $data['tipo_vehiculo']; ?></td>
									<td><?php echo $data['placa']; ?></td>									
									<td><?php echo $data['marca']; ?></td>
									<td><?php echo $data['modelo']; ?></td>
									<td><?php echo $data['color']; ?></td>									
									
									<?php if ($_SESSION['rol'] == 1||$_SESSION['rol'] == 6) { ?>
									<td>									
										<a href="editar_vehiculo.php?id=<?php echo $data['cod_vehiculo'];?>" class="btn btn-success btn-sm">
											<i class='fas fa-edit'></i>										
										</a>
										<form action="eliminar_vehiculo.php?id=<?php echo $data['cod_vehiculo']; ?>" method="post" class="confirmar d-inline">
											<button class="btn btn-danger btn-sm" type="submit"><i class='fas fa-trash-alt'></i> </button>
										</form>
										
									</td>
									<?php } ?>
								</tr>
						<?php }
						} ?>
					</tbody>
				    </table>
			    </div>
            </div>
        </div>          
        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>