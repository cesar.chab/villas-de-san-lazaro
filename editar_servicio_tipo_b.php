 <?php include_once "includes/header.php";
  include "../conexion.php";


//obtenemos el correlativo 
$correlativo_recibo=$_SESSION['correlativo_recibo'];

//obtenemos el correlativo 
$codcasa=$_SESSION['codcasa'];

$codservicio=$_GET['codservicio'];

 $query_servicio = mysqli_query($conexion, "SELECT * FROM servicios where codservicio=$codservicio");
  $result_servicio = mysqli_num_rows($query_servicio);

  if ($result_servicio > 0) {
    $data_servicio = mysqli_fetch_assoc($query_servicio);
}

  if (!empty($_POST)) {
    $alert = "";

    //Si existe el servicio que no esté en blanco las casillas
    if (empty($_POST['servicio']) || empty($_POST['precio']) ) {
      $alert = '<div class="alert alert-danger" role="alert">
                Todo los campos son obligatorios
              </div>';
    } else {
    
  
      $servicio = $_POST['servicio'];
      $precio = $_POST['precio'];
        $tipo_recibo = $_POST['tipo_recibo'];
     

      $usuario_id = $_SESSION['idUser'];

    $query_insert = mysqli_query($conexion, "INSERT INTO servicios_tipo_a(servicio,precio,tipo_recibo,codcasa,correlativo_recibo) values ('$servicio', $precio,'$tipo_recibo',$codcasa,'$correlativo_recibo')");

          $query_update = mysqli_query($conexion, "UPDATE pagos_realizados SET  estado_finalizado=1, tipo_recibo='$tipo_recibo' WHERE  codcasa=$codcasa and estado_finalizado=0");

      if ($query_insert) {
        $alert = '<div class="alert alert-primary" role="alert">
               Pago agregado exitosamente.
              </div>';
      } else {
        $alert = '<div class="alert alert-danger" role="alert">
                Erro, el pago no se pudo agregar.
              </div>';
      }
    }
  }
  ?>

 <!-- Begin Page Content -->
 <div class="container-fluid">

   <!-- Page Heading -->
   <div class="d-sm-flex align-items-center justify-content-between mb-4">
     <h1 class="h3 mb-0 text-gray-800">Confirmar</h1>
      <a href="agregar_pago_lectura_b.php?codcasa=<?php echo $codcasa?>" class="btn btn-primary">Regresar</a>
   </div>

   <!-- Content Row -->
   <div class="row">
     <div class="col-lg-6 m-auto">
       <form action="" method="post" autocomplete="off">
         <?php echo isset($alert) ? $alert : ''; ?>
         


  <div class="form-group">
         <div class="form-group">
           <label for="servicio">Servicio</label>
           <input type="text" placeholder="Ingrese nombre del servicio" name="servicio" id="servicio" class="form-control" value="<?php echo $data_servicio['servicio']?>" readonly="readonly">
         </div>
         <div class="form-group">
           <label for="precio">Precio</label>
           <input type="text" placeholder="Ingrese precio" class="form-control" name="precio" id="precio" value="<?php echo $data_servicio['precio']?>" readonly="readonly">
         </div>

 <div class="form-group">
                    <label>Tipo de Recibo</label>
                   
 <input type="text" placeholder="tipo_recibo" class="form-control" name="tipo_recibo" id="tipo_recibo" value="<?php echo $data_servicio['tipo_recibo']?>" readonly="readonly">

                   </div>


       


         <input type="submit" value="Agregar el pago" class="btn btn-primary">
       </form>
     </div>
   </div>


 </div>
 <!-- /.container-fluid -->

 </div>
 <!-- End of Main Content -->
 <?php include_once "includes/footer.php"; ?>