<?php include_once "includes/header.php"; ?>


<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Lectura de Agua</h1>
		<a href="registro_lectura.php" class="btn btn-primary">Registrar Lectura</a>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="table">
					<thead class="thead-dark">
						<tr>
							<th>No.</th>
							<th>No. Casa</th>
							<th>Mes</th>
							<th>Período</th>
							<th>Lec. Anterior</th>
							<th>Lec.Actual</th>
							<th>Lectura en m³</th>
							<th>Exceso en m³</th>
							<th>Cargo Exceso</th>
							<th>Fecha de Lectura</th>
							<?php if ($_SESSION['rol']== 1 || $_SESSION['rol'] == 4 ||$_SESSION['rol'] == 6) { ?>
							<th>ACCIONES</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						include "../conexion.php";

						//Variable del periodo actual en la página header

						$periodo_actual=date('Y');

						$query = mysqli_query($conexion, "SELECT * FROM lectura where periodo=$periodo_actual and estado=1");
						$result = mysqli_num_rows($query);
						$fila=0;
						
						if ($result > 0) {
							while ($data = mysqli_fetch_assoc($query)) { ?>
								<tr>
									<td><?php echo $fila=$fila+1; ?></td>
									<td><?php echo	$casa=$data['codcasa'];	?></td>
									<td><?php echo $data['mes']; ?></td>
									<td><?php echo $data['periodo']; ?></td>
									<td><?php echo $data['lectura_anterior']-$data['lectura']; ?></td>
									<td><?php echo $data['lectura_actual']; ?></td>
									<td><?php echo $data['lectura']." m³"; ?></td>
									<td><?php echo $data['exceso']." m³"; ?></td>
									<td><?php echo "Q".$data['cargo']; ?></td>
									
									<td><?php echo $data['fecha_lectura']; ?></td>
									
							
										<?php if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 4||$_SESSION['rol'] == 6 ) { ?>
									<td>
									

										<!--<a href="editar_lectura.php?id=<?php echo $data['idcasa']; ?>" class="btn btn-success"><i class='fas fa-edit'></i></a>-->

										<form action="eliminar_lectura.php?idcasa=<?php echo $data['idcasa']; ?>" method="post" class="confirmar d-inline">
											<button class="btn btn-danger" type="submit"><i class='fas fa-trash-alt'></i> </button>
										</form>
									</td>
										<?php } ?>
								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>