<?php include_once "includes/header.php"; ?>


<!DOCTYPE html>
<html lang="es">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <!-- Page Heading -->
<div class="container mt-3">
    <h2> Cierres</h2>
  <button class="btn btn-secondary dropdown-toggle" style="background: #0d2646  " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
    Visualizar Cierres
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
    <li><a class="dropdown-item" href="cierres_dias.php">Cierres diarios</a></li>
    <li><a class="dropdown-item" href="#"  data-toggle="modal" data-target="#semanal_cierres"> Cierres de las semanas</li>
    <li><a class="dropdown-item" href="cierres_meses.php">Cierres de los meses</a></li>
  </ul>
<br>
<br>
  <h2><center> Reportes</center></h2>
  <!-- Button to Open the Modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
    Cheques Anulados
  </button>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">
    Recibos Realizados
  </button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal3">
    Lecturas Realizadas (Agua)
  </button>

   </button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal4">
    Vecinos con pagos atrasados
  </button>


  </button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal5">
    Tasa Municipal
  </button>
    
  </div>


  
  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Cheques Anulados</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>

        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
<form action="pdf_cheque_rechazado.php" method="post">
<center>
 <div class="col">
    <div class="col-lg-6">

   
        <label>Fecha Inicio</label>
        <input type="date" name="inicio" id="inicio" required="" value="<?php  echo $_POST['inicio']?>">
    
</div>
<div class="col-lg-6">


        <label>Fecha Final</label>
        <input type="date" name="cierre" id="cierre" required="" value="<?php  echo $_POST['cierre']?>">

</div>
<div class="col-lg-6">
<button type="submit" class="btn btn-primary" style="background:  #3c8341   ">Reporte en PDF</button>


</center>

 </div>


</div>





</form> 



        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>










 <!-- The Modal RECIBOS REALIZADOS -->
  <div class="modal fade" id="myModal2">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Recibos Realizados (Solo reportados)</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>

        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
<form action="pdf_recibos_realizados.php" method="post">
<center>
 <div class="col">
    <div class="col-lg-6">

   
        <label>Fecha Inicio</label>
        <input type="date" name="inicio" id="inicio" required="" value="<?php  echo $_POST['inicio']?>">
    
</div>
<div class="col-lg-6">


        <label>Fecha Final</label>
        <input type="date" name="cierre" id="cierre" required="" value="<?php  echo $_POST['cierre']?>">

</div>
<div class="col-lg-6">
<button type="submit" class="btn btn-primary" style="background:  #3c8341   ">Reporte en PDF</button>


</center>

 </div>


</div>





</form> 



        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>



<!-- LECTURAS REALIZADAS-->


  <div class="modal fade" id="myModal3">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Lecturas de Agua Realizadas</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>

        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
<form action="pdf_lecturas_realizados.php" method="post">
<center>
 <div class="col">
    <div class="col-lg-6">

   
        <label>Fecha Inicio</label>
        <input type="date" name="inicio" id="inicio" required="" value="<?php  echo $_POST['inicio']?>">
    
</div>
<div class="col-lg-6">


        <label>Fecha Final</label>
        <input type="date" name="cierre" id="cierre" required="" value="<?php  echo $_POST['cierre']?>">

</div>
<div class="col-lg-6">
<button type="submit" class="btn btn-primary" style="background:  #3c8341   ">Reporte en PDF</button>


</center>

 </div>


</div>





</form> 



        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>





<!-- The Modal RECIBOS REALIZADOS -->
  <div class="modal fade" id="myModal4">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Mantenimiento atrasados.</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>

        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
<form action="pdf_vecinos_pagos_atrasados.php" method="post">
<center>
 <div class="col">
    <div class="col-lg-6">

   
        <label>Fecha Inicio</label>
        <input type="date" name="inicio" id="inicio" required="" value="<?php  echo $_POST['inicio']?>">
    
</div>
<div class="col-lg-6">


        <label>Fecha Final</label>
        <input type="date" name="cierre" id="cierre" required="" value="<?php  echo $_POST['cierre']?>">

</div>
<div class="col-lg-6">
<button type="submit" class="btn btn-primary" style="background:  #3c8341   ">Reporte en PDF</button>


</center>

 </div>


</div>





</form> 



        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>












<!-- CIERRES DE LA SEMANA-->


<!-- The Modal RECIBOS REALIZADOS -->
  <div class="modal fade" id="semanal_cierres">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Seleccione una fecha equivalente a una semana.</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>

        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
<form action="pdf_cierres_semanas.php" method="post">
<center>
 <div class="col">
    <div class="col-lg-6">

   
        <label>Fecha Inicio</label>
        <input type="date" name="inicio" id="inicio" required="" value="<?php  echo $_POST['inicio']?>">
    
</div>
<div class="col-lg-6">


        <label>Fecha Final</label>
        <input type="date" name="cierre" id="cierre" required="" value="<?php  echo $_POST['cierre']?>">

</div>
<div class="col-lg-6">
<button type="submit" class="btn btn-primary" style="background:  #3c8341   ">Reporte en PDF</button>


</center>

 </div>


</div>





</form> 



        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>






<!--tasa municipal-->


  <div class="modal fade" id="myModal5">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Tasa Municipal</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>

        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
<form action="pdf_tasa_municipal.php" method="post">
<center>
 <div class="col">
    <div class="col-lg-6">

   
        <label>Fecha Inicio</label>
        <input type="date" name="inicio" id="inicio" required="" value="<?php  echo $_POST['inicio']?>">
    
</div>
<div class="col-lg-6">


        <label>Fecha Final</label>
        <input type="date" name="cierre" id="cierre" required="" value="<?php  echo $_POST['cierre']?>">

</div>
<div class="col-lg-6">
<button type="submit" class="btn btn-primary" style="background:  #3c8341   ">Reporte en PDF</button>


</center>

 </div>


</div>





</form> 



        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>

    <!-- Page Heading -->
<div class="container mt-3">

  <button class="btn btn-secondary dropdown-toggle" style="background: #0d2646  " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
    <i class="far fa-sticky-note"></i>&nbsp;Lecturas y recibos por Usuarios
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
    <li><a class="dropdown-item" href="#"  data-toggle="modal" data-target="#lecturas_por_usuarios">Lectura por Usuarios</a></li>
    <li><a class="dropdown-item" href="#"  data-toggle="modal" data-target="#recibos_por_usuarios">Recibos por Usuarios</a></li>
  
  </ul>




<!-- LECTURAS REALIZADAS POR USUARIOS-->

<!-- The Modal RECIBOS REALIZADOS -->
  <div class="modal fade" id="lecturas_por_usuarios"> 
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Lecturas Por Usuarios</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>

        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
<form action="pdf_lecturas_por_usuarios.php" method="post">
<center>
 <div class="col">
    <div class="col-lg-6">

   
        <label>Fecha Inicio</label>
        <input type="date" name="inicio" id="inicio" required="" value="<?php  echo $_POST['inicio']?>">
    
</div>
<div class="col-lg-6">


        <label>Fecha Final</label>
        <input type="date" name="cierre" id="cierre" required="" value="<?php  echo $_POST['cierre']?>">

</div>

<div class="col-lg-6">


        <label>Seleccione un Usuario</label>
        <select class="form-select" aria-label="Default select example" name="usuario" id="usuario">
     
  
<?php  
$query_usuario=mysqli_query($conexion,"SELECT*FROM usuario where rol=4 or rol=1"); 

$result_usu = mysqli_num_rows($query_usuario);
                     
            if ($result_usu > 0) {
              while ($data = mysqli_fetch_assoc($query_usuario)) 
              { 

                ?>
                    

  <option value="<?php echo $data['idusuario'];?>"><?php echo $data['usuario'];?></option>

<?php

}
}
?>
</select>


</div>


<div class="col-lg-6">
<button type="submit" class="btn btn-primary" style="background:  #3c8341   ">Reporte en PDF</button>


</center>

 </div>


</div>





</form> 



        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>







<!--RECIBOS REALIZADAS POR USUARIOS-->

<!-- The Modal RECIBOS REALIZADOS POR USUARIOS -->
  <div class="modal fade" id="recibos_por_usuarios"> 
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Recibos por Usuarios (Solo reportados)</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>

        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
<form action="pdf_recibos_por_usuarios.php" method="post">
<center>
 <div class="col">
    <div class="col-lg-6">

   
        <label>Fecha Inicio</label>
        <input type="date" name="inicio" id="inicio" required="" value="<?php  echo $_POST['inicio']?>">
    
</div>
<div class="col-lg-6">


        <label>Fecha Final</label>
        <input type="date" name="cierre" id="cierre" required="" value="<?php  echo $_POST['cierre']?>">

</div>

<div class="col-lg-6">


        <label>Seleccione un Usuario</label>
        <select class="form-select" aria-label="Default select example" name="usuario" id="usuario">
     
  
<?php  
$query_usuario=mysqli_query($conexion,"SELECT*FROM usuario where rol=2 or rol=1"); 

$result_usu = mysqli_num_rows($query_usuario);
                     
            if ($result_usu > 0) {
              while ($data = mysqli_fetch_assoc($query_usuario)) 
              { 

                ?>
                    

  <option value="<?php echo $data['idusuario'];?>"><?php echo $data['usuario'];?></option>

<?php

}
}
?>
</select>


</div>


<div class="col-lg-6">
<button type="submit" class="btn btn-primary" style="background:  #3c8341   ">Reporte en PDF</button>


</center>

 </div>


</div>





</form> 



        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
        </div>
        
      </div>
    </div>
  </div>



  
</div>

</body>
</html>





            <?php include_once "includes/footer.php"; ?>
