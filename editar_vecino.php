<?php include_once "includes/header.php";
include "../conexion.php";
if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['num_casa'])  || empty($_POST['nombre_pro'])) {
    $alert = '<div class="alert alert-danger" role="alert">
                Todo los campos son obligatorio
            </div>';
  } else {
    $id= $_GET['id'];
    //datos de propietarios
    $num_casa=$_POST['num_casa'];
      $direccion_casa=$_POST['direccion_casa'];
    $dpi_pro = $_POST['dpi_pro'];
    $nombre_pro = $_POST['nombre_pro'];
    $telefono_pro = $_POST['telefono_pro'];
    $correo_pro = $_POST['correo_pro'];
    $contador_agua = $_POST['num_contador'];
    //datos de inquilinos
    $dpi_inq = $_POST['dpi_inq'];
    $nombre_inq = $_POST['nombre_inq'];
    $telefono_inq = $_POST['telefono_inq'];
    $correo_inq = $_POST['correo_inq'];
    $cant_inq = $_POST['cant_inq'];
    $nit= $_POST['nit'];
    $result = 0;
    $sql_update = mysqli_query($conexion, 
      "UPDATE vecino_inquilino SET 
        num_casa=$num_casa, direccion_casa='$direccion_casa', dpi_pro=$dpi_pro, nombre_pro='$nombre_pro', telefono_pro='$telefono_pro', correo_pro= '$correo_pro',num_contador=$contador_agua,
        dpi_inq=$dpi_inq, nombre_inq='$nombre_inq', telefono_inq='$telefono_inq', correo_inq='$correo_inq',cant_inq=$cant_inq, nit='$nit'
    
      WHERE cod_vecino = $id");

      if ($sql_update) {
        $alert = '<div class="alert alert-success" role="alert">
                    Datos editaso con éxito
                  </div>';
      } else {
        $alert = '<div class="alert alert-alert" role="alert">
                       Error al actualizar datos
                 </div>';
      }
    }
  }
// Mostrar Datos
if (empty($_REQUEST['id'])) {
  header("Location: lista_vecino.php");
}
$idvecino = $_REQUEST['id'];
$sql = mysqli_query($conexion, "SELECT * FROM vecino_inquilino WHERE cod_vecino = $idvecino;");
$result_sql = mysqli_num_rows($sql);
if ($result_sql == 0) {
  header("Location: lista_vecino.php");
} else {
  while ($data = mysqli_fetch_array($sql)) {
    $num_casa = $data['num_casa'];
    $direccion_casa=$data['direccion_casa'];
    $dpi_pro = $data['dpi_pro'];
    $nombre_pro = $data['nombre_pro'];
    $telefono_pro = $data['telefono_pro'];
    $correo_pro = $data['correo_pro'];
    $cant_inq = $data['cant_inq'];

    $dpi_inq= $data['dpi_inq'];
    $nombre_inq = $data['nombre_inq'];
    $telefono_inq = $data['telefono_inq'];
    $correo_inq = $data['correo_inq'];
    $num_contador = $data['num_contador'];
    
       $nit = $data['nit'];
    
    
  }
}
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">


              <!-- Page Heading -->
              <div class="d-sm-flex align-items-center justify-content-between mb-4">
                  <h1 class="h3 mb-0 text-gray-800">Registrar Vecino</h1>
                  <a href="lista_vecino.php" class="btn btn-primary">Regresar</a>
              </div>
            <!-- Content Row -->
          <div class="row">
          <div class="col-lg-10 m-auto">
          <form action="" method="post" autocomplete="off">
              <div class="row">
                <div class="col-md-11">
                    <?php echo isset($alert) ? $alert : ''; ?>
                 </div>
              </div>
                <div class="row">                   
                    <div class="col-md-5"> 
                        <fieldset>
                        <legend>Datos de Propietario</legend>
                        <div class="form-group">
                            <label for="nocasa">Número de casa</label>
                            <input type="number" placeholder="Ingrese número de casa" name="num_casa" id="num_casa" class="form-control" value="<?php echo $num_casa; ?>">
                        </div>
                         <div class="form-group">
                            <label for="direccion_casa">Dirección</label>
                            <input type="text" placeholder="Ingrese número de casa" name="direccion_casa" id="direccion_casa" class="form-control" value="<?php echo $direccion_casa; ?>">
                        </div>
                        <div class="form-group">
                            <label for="dpi">DPI</label>
                            <input type="number" placeholder="Ingrese DPI" name="dpi_pro" id="dpi_pro" class="form-control" value="<?php echo $dpi_pro; ?>">
                        </div>
                        <div class="form-group">
                            <label for="nombre_propietario">Nombre Completo</label>
                            <input type="text" placeholder="Ingrese Nombre" name="nombre_pro" id="nombre_pro" class="form-control" value="<?php echo $nombre_pro; ?>">
                        </div>
                        <div class="form-group">
                            <label for="telefono">Teléfono</label>
                            <input type="text" placeholder="Ingrese Teléfono" name="telefono_pro" id="telefono_pro" class="form-control" value="<?php echo $telefono_pro; ?>">
                        </div>
                        <div class="form-group">
                            <label for="correo_propietario">Correo</label>
                            <input type="email" placeholder="Ingrese Correo" name="correo_pro" id="correo_pro" class="form-control" value="<?php echo $correo_pro; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Cantidad_personas">Total de personas en la vivienda</label>
                            <input type="number" placeholder="Cant Personas" name="cant_inq" id="cant_inq" class="form-control" value="<?php echo $cant_inq; ?>">
                        </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                    <fieldset>
                        <legend>Datos de Inquilino</legend>
                        <div class="form-group">
                            <label for="dpi_inquilino">DPI</label>
                            <input type="number" placeholder="Ingrese DPI" name="dpi_inq" id="dpi_inq" class="form-control" value="<?php echo $dpi_inq; ?>">
                        </div>
                        <div class="form-group">
                            <label for="nombre_inquilino">Nombre Completo</label>
                            <input type="text" placeholder="Ingrese Nombre" name="nombre_inq" id="nombre_inq" class="form-control" value="<?php echo $nombre_inq; ?>">
                        </div>
                        <div class="form-group">
                            <label for="telefono">Teléfono</label>
                            <input type="text" placeholder="Ingrese Teléfono" name="telefono_inq" id="telefono_inq" class="form-control" value="<?php echo $telefono_inq; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Correo">Correo</label>
                            <input type="email" placeholder="Ingrese Correo" name="correo_inq" id="correo_inq" class="form-control" value="<?php echo $correo_inq; ?>">
                        </div>
                        
                        <div class="form-group">
                            <label for="numero_contador">Numeró de contador Agua</label>
                            <input type="number" placeholder="Ingrese Numero de contador" name="num_contador" id="num_contador" class="form-control" value="<?php echo $num_contador; ?>">
                        </div>
                        
                        
                         <div class="form-group">
                            <label for="numero_contador">NIT para el recibo</label>
                            <input type="text" placeholder="Ingrese Número de NIT" name="nit" id="num_nit" class="form-control" value="<?php echo  $nit; ?>" style="background: #c3ead6 ">
                        </div>
                        
                        
                    </fieldset>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="submit" value="Actualizar Datos" class="btn btn-primary">
                       
                    </div>                            
                </div>
            </form>
        </div>
    </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <?php include_once "includes/footer.php"; ?>