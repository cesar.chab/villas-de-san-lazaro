<?php include_once "includes/header.php"; ?>

<?php  //terminar las sesiones de cualquier usuario?>
<?php unset($_SESSION["cod_recibo_a"]);?>
<?php unset($_SESSION["cod_recibo_b"]);?>


                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <h4 class="text-center">Registros Almacenados</h4>
                          
                            </div>

                                          
                           






            
        <!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h4 class="text-center">Pagos Realizados</h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>No.</th>
                            <th>No. de Casa</th>
                            <th>Inquilino</th>
                            <th>Correlativo recibo</th>
                            <th>Tipo</th>
                                 <th>Modo</th>
                                 <th>Número</th>
                                 <th>Banco</th>

                            <th>Fecha Procesado</th>
                            <th>Monto</th>
                           

                            <?php if ($_SESSION['rol'] == 1||$_SESSION['rol'] == 6) { ?>
                            <th>ACCIONES</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include "../conexion.php";


   $query_delete = mysqli_query($conexion, "DELETE FROM pagos_realizados WHERE inquilino =''");
  


                        //comparamos el estado de lectura y si el estado actual ya esta en cola entonces ya no mostrar

                        $query = mysqli_query($conexion, "SELECT * FROM pagos_realizados where codcasa>0 and estado_finalizado=1 and estado_contabilidad=5 and estado_reporte=0 and cheque_rechazado=0 ORDER BY fecha_procesado ASC");
                        $result = mysqli_num_rows($query);
                        $fila=0;
                        if ($result > 0) {
                            while ($data = mysqli_fetch_assoc($query)) { ?>
                                <tr>
                                    <td><?php echo $fila=$fila+1; ?></td>
                                    <td><?php echo $data['codcasa']; ?></td>
                                    <td><?php echo $data['inquilino']; ?></td>
                                    

<td><?php   

                                    if($data['tipo_recibo']=="A")
                                    {


                                    echo $data['id_recibo']; // PONEMOS EL CORRELATIVO O EL ID DEL RECIBO TIPO A 
                                      }
                                      else

                                        {

                                    echo $data['correlativo_recibo']; // PONEMOS EL CORRELATIVO O EL ID DEL RECIBO TIPO A 

                                      }
?>

                                    </td>



                                    
                                     <td><?php echo $data['tipo_recibo']; ?></td>
                                     <td><?php 


                                     if($data['cheque_deposito']==1)
                                     {
                                        echo "Depósito";

                                     }
                                       if($data['cheque_deposito']==2)
                                     {
                                        echo "Cheque";

                                     }
                                       if($data['cheque_deposito']==0)
                                     {
                                        echo "Efectivo";

                                     }

                                     ?>                                 
                                       

                                     </td>
                                     <td><?php
                                      if($data['num_che_bol']==0)
                                      {

                                        echo "---";


                                      }
                                      else
                                      {
                                        echo $data['num_che_bol'];
                                      }




                                      ?></td>



                                    <td>
                                      <?php 

                                      if($data['banco']=='0')
                                      {
                                        echo "---";
                                      }
                                      else
                                      {
                                        echo $data['banco'];
                                      }




                                     ?></td>
                                   
                                    <td><?php echo $data['fecha_procesado']; ?></td>
                                   
                                    
                                        
                                    </td>

                                    <td><?php 

                                    if($data['num_che_bol']==0) // si es efectivo mostrar efectivo sino mostrar valor de cheque/depósito
                                    {
                                      echo $data['total_pagado'];
                                    }
                                    else
                                    {
                                      echo $data['valor'];
                                    }


                                     ?></td>
                                    
                            
                                        <?php if ($_SESSION['rol'] == 1||$_SESSION['rol'] == 6) { ?>
                                    <td>
                                    


                                        <a href="visualizar_almacenados.php?id=<?php echo $data['idcasa']; ?>" class="btn btn-visualizar">
                                        	<i class="fa fa-eye" aria-hidden="true"></i>Visualizar</a>

                                       
                                    </td>
                                        <?php } ?>
                                </tr>
                        <?php }
                        } ?>
                    </tbody>

                </table>
            </div>

        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


            <?php include_once "includes/footer.php"; ?>
