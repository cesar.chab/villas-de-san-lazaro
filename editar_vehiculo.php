<?php include_once "includes/header.php";
include "../conexion.php";
if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['num_casa']) || empty($_POST['placa']) || empty($_POST['marca']) || empty($_POST['color']) || empty($_POST['modelo'])) {
    $alert = '<div class="alert alert-danger" role="alert">
                Todo los campos son obligatorio
            </div>';
  } else {
    $id= $_GET['id'];
    //datos de propietarios
    
    $tipo_vehiculo=strtoupper($_POST['tipovehiculo']);
    $placa=strtoupper($_POST['placa']);
    $marca=strtoupper($_POST['marca']);
    $color=strtoupper($_POST['color']);
    $modelo=strtoupper($_POST['modelo']);
    $result = 0;
    $sql_update = mysqli_query($conexion, 
      "UPDATE vehiculos SET 
         tipo_vehiculo='$tipo_vehiculo', placa='$placa', marca='$marca', modelo=$modelo, color='$color'
      WHERE cod_vehiculo = $id");

      if ($sql_update) {
        $alert = '<div class="alert alert-success" role="alert">
                    Datos editaso con éxito
                  </div>';
      } else {
        $alert = '<div class="alert alert-danger" role="alert">
                       Error al actualizar datos
                 </div>';
      }
    }
  }
// Mostrar Datos
if (empty($_REQUEST['id'])) {
  header("Location: registro_vehiculo.php");
}
$idvehiculo = $_REQUEST['id'];
$sql = mysqli_query($conexion, "SELECT * FROM vehiculos WHERE cod_vehiculo = $idvehiculo;");
$result_sql = mysqli_num_rows($sql);
if ($result_sql == 0) {
  header("Location: registro_vehiculo.php");
} else {
  while ($data = mysqli_fetch_array($sql)) {
    $num_casa = $data['num_casa'];
    $tipo_vehiculo = $data['tipo_vehiculo'];
    $placa = $data['placa'];
    $marca = $data['marca'];
    $modelo = $data['modelo'];
    $color = $data['color'];
  }
}
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">


              <!-- Page Heading -->
              <div class="d-sm-flex align-items-center justify-content-between mb-4">
                  <h1 class="h3 mb-0 text-gray-800">Registro de Vehículos</h1>
                  <a href="lista_vecino.php" class="btn btn-primary">Regresar</a>
              </div>
            <!-- Content Row -->
          <div class="row">
          <div class="col-lg-10 m-auto">
          <form action="" method="post" autocomplete="off">
              <div class="row">
                <div class="col-md-5 m-auto">
                    <?php echo isset($alert) ? $alert : ''; ?>
                 </div>
              </div>
                <div class="row">                   
                    <div class="col-md-5 m-auto"> 
                    <div class="card card-body">
                        <h4 class="text-primary text-center">DATOS VEHÍCULO</h4>
                        
                            <div class="form-group">
                                <label for="num_casa">NÚMERO DE CASA</label>
                                <input type="number" placeholder="Número de casa" name="num_casa" id="num_casa" class="form-control" value="<?php echo $num_casa?>" readonly>
                            </div>
                            <div class="form-group">
                                <label for="vehiculo">TIPO</label>
                                <select class="form-group" aria-label="Default select example" name="tipovehiculo">
                                    <option selected value="automovil">AUTOMÓVIL</option>
                                    <option value="moto">MOTOCICLETA</option>
                                </select>
                            </div>
                            <div class="form-group">                                
                                <input type="text" value="<?php echo $placa?>" placeholder="Número de Placa sin guión" name="placa" id="placa_uno" class="form-control" style="text-transform:uppercase;">
                            </div>
                            <div class="form-group">                                
                                <input type="text" value="<?php echo $marca?>" placeholder="Marca del vehículo  " name="marca" id="marca_uno" class="form-control" style="text-transform:uppercase;">
                            </div>
                            <div class="form-group">                            
                                <input type="text" value="<?php echo $color?>" placeholder="Color del vehículo" name="color" id="color_uno" class="form-control" style="text-transform:uppercase;">
                            </div>
                            <div class="form-group">        
                                <input type="number" value="<?php echo $modelo?>" placeholder="Modelo del Vehículo" name="modelo" id="modelo" class="form-control" style="text-transform:uppercase;">
                            </div>
                            <div class="row">
                            <div class="col-md-5 m-auto">
                                    <input type="submit" value="Actualizar Datos" class="btn btn-primary">
                                
                            </div>                            
                </div>
                        
                    </div>
                    </div>                    
                </div>
                
            </form>
        </div>
    </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <?php include_once "includes/footer.php"; ?>