 <?php include_once "includes/header.php";
  include "../conexion.php";
  if (!empty($_POST)) {
    $alert = "";

    //Si existe el servicio que no esté en blanco las casillas
    if (empty($_POST['servicio']) || empty($_POST['precio']) ) {
      $alert = '<div class="alert alert-danger" role="alert">
                Todo los campos son obligatorios
              </div>';
    } else {
    
   $codigo_servicio = $_POST['codigo_servicio'];
      $servicio = $_POST['servicio'];
      $precio = $_POST['precio'];
        $tipo_recibo = $_POST['tipo_recibo'];
     

      $usuario_id = $_SESSION['idUser'];

    $query_insert = mysqli_query($conexion, "INSERT INTO servicios(codigo_servicio,servicio,precio,tipo_recibo) values ($codigo_servicio,'$servicio', $precio,'$tipo_recibo')");

      if ($query_insert) {
        $alert = '<div class="alert alert-primary" role="alert">
               Servicios Registrado
              </div>';
      } else {
        $alert = '<div class="alert alert-danger" role="alert">
                Error al registrar el servicio
              </div>';
      }
    }
  }
  ?>

 <!-- Begin Page Content -->
 <div class="container-fluid">

   <!-- Page Heading -->
   <div class="d-sm-flex align-items-center justify-content-between mb-4">
     <h1 class="h3 mb-0 text-gray-800">Registro de Servicios</h1>
     <a href="lista_servicios.php" class="btn btn-primary">Visualizar Servicio</a>
   </div>

   <!-- Content Row -->
   <div class="row">
     <div class="col-lg-6 m-auto">
       <form action="" method="post" autocomplete="off">
         <?php echo isset($alert) ? $alert : ''; ?>
         


  <div class="form-group">


    <div class="form-group">
           <label for="codigo_servicio">Código del Servicio</label>
           <input type="number" placeholder="Ingrese código del servicio" name="codigo_servicio" id="codigo_servicio" class="form-control" required="required">
         </div>
         <div class="form-group">
           <label for="servicio">Servicio</label>
           <input type="text" placeholder="Ingrese nombre del servicio" name="servicio" id="servicio" class="form-control">
         </div>
         <div class="form-group">
           <label for="precio">Precio</label>
           <input type="text" placeholder="Ingrese precio" class="form-control" name="precio" id="precio">
         </div>

 <div class="form-group">
                    <label>Tipo de Recibo</label>
                    <select name="tipo_recibo" id="tipo_recibo" class="form-control">
                        <?php
                        $query_tipo_recibo = mysqli_query($conexion, " select * from tipo_recibo");
                        mysqli_close($conexion);
                        $resultado_tipo_recibo = mysqli_num_rows($query_tipo_recibo);
                        if ($resultado_tipo_recibo > 0) {
                            while ($tipo_recibo_visual = mysqli_fetch_array($query_tipo_recibo)) {
                        ?>
                                <option value="<?php echo $tipo_recibo_visual["tipo_recibo"]; ?>"><?php echo $tipo_recibo_visual["tipo_recibo"] ?></option>
                        <?php

                            }
                        }

                        ?>
                    </select></div>


       


         <input type="submit" value="Registrar Servicio" class="btn btn-primary">
       </form>
     </div>
   </div>


 </div>
 <!-- /.container-fluid -->

 </div>
 <!-- End of Main Content -->
 <?php include_once "includes/footer.php"; ?>